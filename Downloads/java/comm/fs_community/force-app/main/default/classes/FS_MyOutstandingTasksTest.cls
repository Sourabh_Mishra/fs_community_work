@isTest
public class FS_MyOutstandingTasksTest {
    @testSetup
    private static void createData(){
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];
        User adminUser = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' Limit 1];
        adminUser.UserRoleId = userRole.Id;
        update adminUser;
        System.runAs(adminUser){
            
            fsCore__User_Permission__c userPerm = fsCore.TestHelperSystem.getTestUserPermission(adminUser.Id, true, true, true);
            insert userPerm;
            System.assert(userPerm.Id != null, 'User Permission Creation');
            
            Account accObj = new Account();
            accObj.Name = 'Test Account';
            insert accObj;
            
            Contact conObj = new Contact();
            conObj.FirstName = 'Test';
            conObj.LastName = 'User';
            conObj.AccountId = accObj.Id;
            insert conObj;
            
            Id profileId = [SELECT Id FROM Profile WHERE Name='Custom Community Plus User'].Id;
            String uniqueUserName = 'communityUser' + DateTime.now().getTime() + '@testorg.com';
            User usr = new User(LastName = 'USER',
                                FirstName='TEST',
                                Alias = 'UTEST',
                                Email = 'dummy.user@test.com',
                                Username = uniqueUserName,
                                ProfileId = profileId,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US',
                                ContactId = conObj.Id
                               );
            insert usr;
            
            List<Task> taskList = new List<Task>();
            Task taskObj = new Task();
            taskObj.Priority = 'Normal';
            taskObj.Status = 'Not Started';
            taskObj.Subject = 'Email';
            taskObj.ActivityDate = Date.today();
            taskObj.WhoId = conObj.Id;
            taskObj.WhatId = accObj.Id;
            taskObj.OwnerId = usr.Id;
            taskList.add(taskObj);
            
            Task taskObj1 = new Task();
            taskObj1.Priority = 'High';
            taskObj1.Status = 'Completed';
            taskObj1.Subject = 'Send Quote';
            taskObj1.ActivityDate = Date.today();
            taskObj1.WhoId = conObj.Id;
            taskObj1.OwnerId = usr.Id;
            taskList.add(taskObj1);
            
            Task taskObj2 = new Task();
            taskObj2.Priority = 'Normal';
            taskObj2.Status = 'In Progress';
            taskObj2.Subject = 'Send Letter';
            taskObj2.ActivityDate = Date.today();
            taskObj2.OwnerId = usr.Id;
            taskList.add(taskObj2);
            
            insert taskList;
            System.assertEquals(3, taskList.size());
        }
    }
    
    @isTest
    private static void getDataTest(){
        User u = [SELECT Id FROM User Where Alias = 'UTEST'];
        System.runAs (new User(Id = u.Id) ) {
            List<FS_TaskSchemaClass> resultList = FS_MyOutstandingTasksController.getData();
            System.assertEquals(2, resultList.size()); //2 because 3rd has status as 'Completed'
        }
    }
    @isTest
    private static void getRecordDetailTest(){
        
        User u = [SELECT Id FROM User Where Alias = 'UTEST'];        
        System.runAs (new User(Id = u.Id) ) {
            Id recId = [SELECT Id FROM Task WHERE Subject='Email'].Id;
            FS_TaskSchemaClass resultObj = FS_MyOutstandingTasksController.getcurrentRecordDetails(recId);
            System.assertEquals('Not Started', resultObj.status);
            System.assertEquals('Test User', resultObj.Name);
            System.assertEquals('Normal', resultObj.priority);
            System.assertEquals('Account', resultObj.relatedTo);
            
            recId = [SELECT Id FROM Task WHERE Subject='Send Letter'].Id;
            FS_TaskSchemaClass resultObj1 = FS_MyOutstandingTasksController.getcurrentRecordDetails(recId);
            System.assertEquals('In Progress', resultObj1.status);
            System.assertEquals('--', resultObj1.Name);
            System.assertEquals('Normal', resultObj1.priority);
            System.assertEquals('--', resultObj1.relatedTo);
            
            
            recId = null;
            FS_TaskSchemaClass resultObj2 = FS_MyOutstandingTasksController.getcurrentRecordDetails(recId);
            System.assert(resultObj2==null);
        }
    }
}