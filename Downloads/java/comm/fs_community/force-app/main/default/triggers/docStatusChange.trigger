trigger docStatusChange on fsCore__Lending_Document_Record__c (after update,after insert) {
    String newStatus=null;
    String oldStatus=null;
    String lendingApplicationNumber=null;
    String typeOfDoc=null;
    
    List<fsCore__Lending_Document_Record__c> cdocs=[Select Id,fsCore__Status__c,fsCore__Lending_Application_Number__r.Name,fsCore__Type__c from fsCore__Lending_Document_Record__c where Id=:Trigger.New];
    
    for(fsCore__Lending_Document_Record__c cdoc:cdocs)
    {
        newStatus=cdoc.fsCore__Status__c;
        lendingApplicationNumber=cdoc.fsCore__Lending_Application_Number__r.Name;
        typeOfDoc=cdoc.fsCore__Type__c;
        
    }
    System.debug('newStatus'+newStatus);
    System.debug('typeOfDoc'+typeOfDoc);
    System.debug('lendingApplicationNumber'+lendingApplicationNumber);
    
    String capp=[Select Id,Name from Client_Application__c where Lending_Application__r.Name=:lendingApplicationNumber].Name;
    System.debug('capp:'+capp);
    
    List<Client_Document__c> clist=new List<Client_Document__c>();
    
    List<Client_Document__c> cdocc=[Select Status__c,OwnerId,Client_Application_Number__c from Client_Document__c where Client_Application_Number__c=:capp AND Type__c=:typeOfDoc];
    System.debug('cdocc is:'+cdocc);
    for(Client_Document__c cdocu:cdocc)
    {
        cdocu.Status__c=newStatus;
        clist.add(cdocu);
    }
    System.debug('clist'+clist);
    List<Task> tasklist=new List<Task>();
    for(Client_Document__c cdocus:cdocc)
    {
     Task newTask=new Task();
              newTask.Subject = 'Your '+typeOfDoc+' for appl:'+''+cdocus.Client_Application_Number__c+' '+'is'+' '+newStatus;
              newTask.WhatId = cdocus.Id;

              newTask.OwnerId = cdocus.OwnerId;
              newTask.Type = CommunityConstants.TASK_TYPE_EVENT;
              newTask.Priority=CommunityConstants.TASK_PRIORITY_HIGH;
              newTask.Status=CommunityConstants.TASK_STATUS_COMPLETED;
              newTask.ActivityDate=System.today();
              newTask.fsCore__Source_Activity_ID__c=newStatus;
              tasklist.add(newTask);
    }
    update clist;
    insert tasklist;
    
    
}