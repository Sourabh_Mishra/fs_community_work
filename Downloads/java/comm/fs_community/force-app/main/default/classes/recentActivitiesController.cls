public class recentActivitiesController {

    
        @AuraEnabled
        public static List<FS_TaskSchemaClass> getDataList(){
        Id currUser = UserInfo.getUserId();
        List<Task> tasksList = [SELECT Id,Priority,Status,Subject,ActivityDate,Who.Name,WhoId,fsCore__Source_Activity_ID__c FROM Task WHERE Status='Completed'AND OwnerId =:currUser ORDER BY LastModifiedDate DESC limit 6]; 
        
        List<FS_TaskSchemaClass> schemaTaskList = new List<FS_TaskSchemaClass>();
        for(Task taskObj : tasksList){
            System.debug('\n*************************************');
            System.debug('REC ID: '+taskObj.Id);    
            System.debug('SUBJECT: '+taskObj.Subject);
            System.debug('PRIORITY: '+taskObj.Priority);
            System.debug('STATUS: '+taskObj.Status);
            System.debug('DUE DATE: '+taskObj.ActivityDate);
            System.debug('NAME: '+taskObj.Who.Name);				
            System.debug('\n*************************************\n');
            
            FS_TaskSchemaClass taskSchemaObj = new FS_TaskSchemaClass();
            taskSchemaObj.subject = taskObj.Subject;
            taskSchemaObj.priority = taskObj.Priority;
            taskSchemaObj.status = taskObj.Status;
            taskSchemaObj.dueDate = taskObj.ActivityDate;
            taskSchemaObj.recordId = taskObj.Id;
            taskSchemaObj.relatedTo =taskObj.fsCore__Source_Activity_ID__c;
          
            if(taskObj.WhoId == null){
                taskSchemaObj.Name = '--';
            }
            else{
                taskSchemaObj.Name = taskObj.Who.Name;
            }
            schemaTaskList.add(taskSchemaObj);
        }
        System.debug('Tasks object list: '+schemaTaskList);
        return schemaTaskList;          
    }
    
}