({
    handleInit : function(component, event, helper) {
        
        helper.doInit(component, event, helper); 
        console.log('Doc status is:'+component.get("v.docStatus"));
        console.log('Doc status is:'+component.get("v.recId"));
        
    },
    eventFired:function(component,event,helper){
        console.log('Event is Fired');
        helper.doInit(component, event, helper);
    },
   
    handleUpdateContact: function(component, event, helper) {
        helper.updateContact(component, event, helper);
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.loadingSpinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.loadingSpinner", false);
    },
    
    
})