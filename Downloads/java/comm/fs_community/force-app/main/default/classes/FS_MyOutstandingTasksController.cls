public without sharing class FS_MyOutstandingTasksController {
    
    @AuraEnabled
    public static List<FS_TaskSchemaClass> getData(){
        Id currUser = UserInfo.getUserId();
        List<Task> tasksList = [SELECT Id,Priority,Status,Subject,ActivityDate,Who.Name,WhoId,fsCore__Source_Activity_ID__c FROM Task WHERE Status!='Completed'AND OwnerId =:currUser ORDER BY CreatedDate ];
        
        List<FS_TaskSchemaClass> schemaTaskList = new List<FS_TaskSchemaClass>();
        for(Task taskObj : tasksList){
            System.debug('\n*************************************');
            System.debug('REC ID: '+taskObj.Id);    
            System.debug('SUBJECT: '+taskObj.Subject);
            System.debug('PRIORITY: '+taskObj.Priority);
            System.debug('STATUS: '+taskObj.Status);
            System.debug('DUE DATE: '+taskObj.ActivityDate);
            System.debug('NAME: '+taskObj.Who.Name);				
            System.debug('\n*************************************\n');
            
            FS_TaskSchemaClass taskSchemaObj = new FS_TaskSchemaClass();
            taskSchemaObj.subject = taskObj.Subject;
            taskSchemaObj.priority = taskObj.Priority;
            taskSchemaObj.status = taskObj.Status;
            taskSchemaObj.dueDate = taskObj.ActivityDate;
            taskSchemaObj.recordId = taskObj.Id;
            taskSchemaObj.relatedTo =taskObj.fsCore__Source_Activity_ID__c;
          
            if(taskObj.WhoId == null){
                taskSchemaObj.Name = '--';
            }
            else{
                taskSchemaObj.Name = taskObj.Who.Name;
            }
            schemaTaskList.add(taskSchemaObj);
        }
        System.debug('Tasks object list: '+schemaTaskList);
        return schemaTaskList;          
    }
    
    @AuraEnabled
    public static FS_TaskSchemaClass getcurrentRecordDetails(Id recId)
    {
        if(recId!=null){
            Task result = [SELECT Id,Priority,Status,Subject,ActivityDate,Who.Name,WhatId,WhoId,OwnerId,CreatedById,LastModifiedById FROM Task WHERE Id=:recId];
            String assigneeName = [SELECT Name FROM User Where id=:result.OwnerId].Name;
            String createdBy = [SELECT Name FROM User Where id=:result.CreatedById].Name;
            String lastModifiedBy = [SELECT Name FROM User Where id=:result.LastModifiedById].Name;
            String relatedObjectName;
            if(result.WhatId!=null){
                Id sampleid = result.WhatId; 
                relatedObjectName = sampleid.getsobjecttype().getDescribe().getLabel();
            }
            else{
                relatedObjectName = '--'; 
            }
            
            FS_TaskSchemaClass taskSchemaObj = new FS_TaskSchemaClass();
            taskSchemaObj.subject = result.Subject;
            taskSchemaObj.priority = result.Priority;
            taskSchemaObj.status = result.Status;
            taskSchemaObj.dueDate = result.ActivityDate;
            taskSchemaObj.recordId = result.Id;
            taskSchemaObj.relatedTo = relatedObjectName;
            taskSchemaObj.assignedTo = assigneeName;
            taskSchemaObj.createdBy = createdBy;
            taskSchemaObj.lastModifiedBy = lastModifiedBy;
            if(result.WhoId == null){
                taskSchemaObj.Name = '--';
            }
            else{
                taskSchemaObj.Name = result.Who.Name;
            }
            
            System.debug('Id based task Object record '+taskSchemaObj);
            return taskSchemaObj;
        }
        else{
            System.debug('Id is null');
            return null;
        }
    }
}