({
    MAX_FILE_SIZE: 10000000,
    CHUNK_SIZE: 75000,
    
    doInit: function(component,event,helper){
        var resultMsg = sessionStorage.getItem('pageTransfer');
        console.log('1'+resultMsg);
        var applicationNumber = JSON.parse(resultMsg).appNumber; 
        console.log('2'+applicationNumber);
        component.set("v.applicationNumbers",applicationNumber);
       
        var action2 = component.get("c.getEmploymentIdFromApplication");
       action2.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set('v.empId',response.getReturnValue());
            }
            else{
                var error = response;
                console.log('Error in getEmploymentIdFromApplication'+error);
            }
        });
        var action3 = component.get("c.getIncomeRecordsFromApplication");
        action3.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                if(response.getReturnValue().length != 0){
                console.log('incomeRecords success, length:  '+response.getReturnValue().length);
                component.set('v.incomeRecords',response.getReturnValue());
                }else{
                    var action1 = component.get("c.getIncomeRecTypeId");
                    action1.setCallback(this, function(response1){
                        var state = response1.getState(); 
                        if(state === "SUCCESS"){
                            console.log('getIncomeRecTypeId success: '+response1.getReturnValue());
                            component.set("v.incomeRecTypeId",response1.getReturnValue());
                        }
                        else{
                            console.log('error in getIncomeRecTypeId');
                        }
                    });
                    $A.enqueueAction(action1);
                }
            }
            else{
                var error = response.getError();
                console.log('Error in getIncomeRecordsFromApplication'+error);
            }
        });
        
        var action10 = component.get("c.getLiabilityRecordsFromApplication");
       action10.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                if(response.getReturnValue().length != 0){
                    console.log('liabilityRecords success, length:  '+response.getReturnValue().length);
                component.set('v.liabilityRecords',response.getReturnValue());
                }else{
                    var action1 = component.get("c.getLiabilityRecTypeId");
                    action1.setCallback(this, function(response1){
                        var state = response1.getState(); 
                        if(state === "SUCCESS"){
                            console.log('getLiabilityRecTypeId success: '+response1.getReturnValue());
                            component.set("v.liabilityRecTypeId",response1.getReturnValue());
                        }
                        else{
                            console.log('error in getLiabilityRecId');
                        }
                    });
                    $A.enqueueAction(action1);
                }
            }
            else{
                var error = response;
                console.log('Error in getLiabilityRecordsFromApplication'+error);
            }
        });
        
        var action4 = component.get("c.getApplicationIdFromName");
        action4.setParams({
            appNum : applicationNumber
        })
        action4.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set('v.appId',response.getReturnValue()); 
            }
            else{
                var error = response;
                console.log('Error in getApplicationIdFromName'+error);
            }
        });

        $A.enqueueAction(action2);
        $A.enqueueAction(action3);
        $A.enqueueAction(action10);
        $A.enqueueAction(action4);
       
    },
    
        updateContact: function(component, event, helper) {  
        var action = component.get("c.updateContact");
        console.log('updateContact called');
        console.log('Contact---'+component.get('v.contact'));
        action.setParams({
            pCon : component.get('v.contact')
        });
        console.log('params passed..');
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state==='SUCCESS'){
                console.log('contact update success');
            }
            else{
                console.log('update fail');
                console.log(response);
                var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": "Please check your details before submitting",
            "type": "error"
        });
        toastEvent.fire();
            }
        });
        $A.enqueueAction(action); 
        
       },
    showSpinner : function(component, event){
        var spinner = component.find("waitSpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner : function(component, event){
        var spinner = component.find("waitSpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
       
})