({
    handleInit :function(component, event, helper){
        helper.doInit(component, event, helper);
    },
    handleRowAction: function (component, event, helper) {
        helper.executeRowAction(component, event, helper);
    },
    handleNavigation : function (component, event, helper) {
        helper.navToPage(component, event, helper);
    },
    toggle : function(component, event, helper) {
        var toggleSection = component.find('toggle');
        $A.util.toggleClass(toggleSection, 'slds-show');  
        $A.util.toggleClass(toggleSection, 'slds-hide');
    },
})