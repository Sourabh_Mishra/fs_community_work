public class ClientToLendingApplication {
    public static Date getPaymentStartDate(Date pLoanStartDate, String pPaymentCycle){
        return ClientApplicationUtil.getPaymentStartDate(pLoanStartDate, pPaymentCycle);
    }
    
    @AuraEnabled
    public static fsCore__Lending_Application__c convertClientApp(Id clientAppId){
        Client_Application__c clientApp = [SELECT Id, Name
                                           ,Loan_Amount__c
                                           ,Payment_Cycle__c
                                           ,Primary_Customer_Account__c
                                           ,Primary_Customer_Contact__c
                                           ,Product_Family__c 
                                           ,Purpose__c
                                           ,Status__c
                                           ,Application_Date__c
                                           ,OwnerId
                                           ,Product__c
                                           ,LastModifiedDate
                                           ,Product_Name__c
                                           ,Company_Name__c
                                           ,Branch_Name__c
                                           from Client_Application__c where Id=:clientAppId];
        
        List<Client_Application_Conversion_Mapping__mdt> clientAppMappings = [select Id, Default_Branch_Name__c
                                                                              ,Mapped_Company_Name__c
                                                                              ,Mapped_Maximum_Rate__c
                                                                              ,Mapped_Minimum_Rate__c
                                                                              ,Mapped_Rate__c
                                                                              ,Mapped_Product_Family__c
                                                                              ,Mapped_Requested_Number_Of_Payments__c
                                                                              ,Mapped_Requested_Payment_Cycle__c
                                                                              ,Mapped_Number_Of_Payments__c
                                                                              FROM Client_Application_Conversion_Mapping__mdt where Is_Active__c=true];
       
        fsCore.ObjectRecordMapper appRecordMapper = new fsCore.ObjectRecordMapper(CommunityConstants.CLIENT_APP_LENDING_APP_MAPPING);
        fsCore__Lending_Application__c lendingApp = (fsCore__Lending_Application__c)appRecordMapper.getTargetRecord(clientApp);

        Date loanStartDate = date.newinstance(clientApp.LastModifiedDate.year(), clientApp.LastModifiedDate.month(), clientApp.LastModifiedDate.day());
        lendingApp.fsCore__Application_Date__c = date.newinstance(clientApp.LastModifiedDate.year(), clientApp.LastModifiedDate.month(), clientApp.LastModifiedDate.day());
        lendingApp.fsCore__Product_Family__c = clientAppMappings[0].Mapped_Product_Family__c;
        lendingApp.fsCore__Requested_Payment_Start_Date__c = getPaymentStartDate(loanStartDate,clientApp.Payment_Cycle__c);
         String loanType = clientApp.Product__c;
        System.debug('--LoanType----:   '+loanType);
        fsCore__Product_Setup__c prodObj = [SELECT Id, Name FROM fsCore__Product_Setup__c WHERE Name = :clientApp.Product__c];
        System.debug('--prodObj----'+prodObj); 
        lendingApp.fsCore__Product_Name__c = prodObj.Id;
       // lendingApp.fsCore__Financed_Amount__c = clientApp.Loan_Amount__c;   //??
        lendingApp.fsCore__Requested_Payment_Cycle__c = clientAppMappings[0].Mapped_Requested_Payment_Cycle__c;
        lendingApp.fsCore__Number_Of_Payments__c = clientAppMappings[0].Mapped_Number_Of_Payments__c;
        lendingApp.fsCore__Rate__c = clientAppMappings[0].Mapped_Rate__c;
        lendingApp.fsCore__Rate_Minimum__c =clientAppMappings[0].Mapped_Minimum_Rate__c;
        lendingApp.fsCore__Rate_Maximum__c = clientAppMappings[0].Mapped_Maximum_Rate__c;
        lendingApp.fsCore__Requested_Number_Of_Payments__c = clientAppMappings[0].Mapped_Requested_Number_Of_Payments__c;
        lendingApp.Client_Application__c = clientApp.Id;
       // Id lendingid=lendingApp.id;
        //System.debug('lendingid is:'+lendingid);
        insert lendingApp;
        system.debug('Application Insert successful---  '+lendingApp); 
        
        clientApp.Lending_Application__c = lendingApp.Id;
        clientApp.Application_Status__c = CommunityConstants.SUBMITTED;
        // TO MAKE THE CLIENT APP READ-ONLY
      /*  Approval.LockResult lr = Approval.lock(clientApp, false);
        if (lr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully locked account with ID: ' + lr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : lr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Account fields that affected this error: ' + err.getFields());
        }
    }
*/
        update clientApp;
        Id lendingid=lendingApp.id;
        System.debug('lendingid is:'+lendingid);
        // Id docid=clientApp.Id;
        //System.debug('docid is:'+docid);
        List<Client_Document__c> clientDocuments=[Select Id,
                                                     Type__c,
                                                     Status__c,
                                                     Client_Application_Number__c,
                                                     Document_Classification__c ,
                                                     Client_Application__c
                                                     from Client_Document__c where Client_Application_Number__c=: clientApp.Name and Status__c= :CommunityConstants.SUBMITTED];
        
        List<Client_Document__c> statusUpdate=new List<Client_Document__c>();
        
        List<fsCore__Document_Setup__c> newSetUps=new List<fsCore__Document_Setup__c>();
        List<fsCore__Lending_Document_Record__c> newDocuments=new List<fsCore__Lending_Document_Record__c>();
        
        for(Client_Document__c clientdocs:clientDocuments)
        {
        Id docid=createDocumentSetUp(clientdocs.Type__c,clientdocs.Document_Classification__c);
        addLendingDocs(docid,clientdocs,lendingid );
        
        }
        
        if(clientDocuments.size()>0)
        {
            for(Client_Document__c cd:clientDocuments)
            {
                cd.Status__c=CommunityConstants.PENDING_REVIEW;
                statusUpdate.add(cd);
            }
        }
       update statusUpdate;
       return lendingApp;  

    }
    
    
    public static void addLendingDocs(Id docid,Client_Document__c clientdocs,Id lendingid)
    {
        fsCore.ObjectRecordMapper RecordMapper = new fsCore.ObjectRecordMapper(CommunityConstants.CLIENT_DOC_LENDING_DOC_MAPPING);
        fsCore__Lending_Document_Record__c clientdocu = (fsCore__Lending_Document_Record__c)RecordMapper.getTargetRecord(clientdocs);
        clientdocu.fsCore__Lending_Application_Number__c=lendingid;
        clientdocu.fsCore__Document_Name__c=docid;
        System.debug('client docu is :'+clientdocu);
        insert clientdocu;
        
        Id clink= [select ContentDocumentId from ContentDocumentLink 
                   where LinkedEntityId =:clientdocs.id].ContentDocumentId;
        ContentDocumentLink cDocLinks = new ContentDocumentLink();
        cDocLinks.ContentDocumentId = clink;
        cDocLinks.LinkedEntityId = clientdocu.id;
        cDocLinks.ShareType = 'I';
        cDocLinks.Visibility = CommunityConstants.DOC_VISIBILITY_ALL_USERS;  
        insert cDocLinks;
       
    }
    
    
       public static Id createDocumentSetUp(String setUpType,String SetUpClassification)
        {
           
        Id docSetUp=[Select Id from fsCore__Document_Setup__c where Name=:setUpType].id;
        System.debug('docSetUp is :'+docSetUp);

        return docSetUp;
            
          
       /*fsCore__Document_Setup__c cdocs=new fsCore__Document_Setup__c();
        cdocs.fsCore__Is_Active__c=true;
        cdocs.fsCore__Default_Company_Availability__c='Available';
        cdocs.fsCore__Default_Product_Availability__c='Available';
        cdocs.fsCore__Default_Selection_Rule_Result__c='Available';
        cdocs.Name=setUpType;
        cdocs.fsCore__Document_Classification__c=SetUpClassification;
        Integer randomNumber = Integer.valueof((Math.random() * 100));
        cdocs.fsCore__Document_Code__c=setUpType+randomNumber;
        cdocs.fsCore__Is_Lease__c=true;
        cdocs.fsCore__Selection_Order__c=0;
        cdocs.fsCore__Line_Of_Business__c='Origination';
        cdocs.fsCore__Access_Level__c='Edit';
        insert cdocs;
        return cdocs.id;*/
           }
}