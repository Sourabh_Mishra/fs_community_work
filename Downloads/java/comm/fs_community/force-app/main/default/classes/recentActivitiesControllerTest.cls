@isTest
public class recentActivitiesControllerTest {
   @testSetup
    private static void createTestData(){
        
            
         
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];
        User adminUser = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' Limit 1];
        adminUser.UserRoleId = userRole.Id;
        update adminUser;
        
        System.runAs(adminUser){
            
            fsCore__User_Permission__c userPerm = fsCore.TestHelperSystem.getTestUserPermission(adminUser.Id, true, true, true);
            insert userPerm;
            System.assert(userPerm.Id != null, 'User Permission Creation'); 
            
            Account acc = new Account();
            acc.Name= 'Test Acc';
            insert acc;
            system.debug('------1');
            
            Contact con = new Contact();
            con.LastName='Test';
            con.AccountId = acc.Id;
            insert con;
            system.debug('------2');
            
            Id profileId = [SELECT Id FROM Profile WHERE Name='Custom Community Plus User'].Id;
            
            String uniqueUserName;
            uniqueUserName = 'andy' + DateTime.now().getTime() + '@testorg.com';
            User usr = new User(
                LastName = 'User1',
                FirstName='Test',
                Alias = 'tUOne',
                Email = 'andy.user@test.com',
                Username = uniqueUserName,
                ProfileId = profileId,
                TimeZoneSidKey = 'GMT',
                LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US',
                ContactId = con.Id
            );
            insert usr;
            system.debug('-----3 User '+usr);
            
            List<Task> taskList = new List<Task>();
            Task taskObj = new Task();
            taskObj.Priority = 'Normal';
            taskObj.Status = 'Not Started';
            taskObj.Subject = 'Email';
            taskObj.ActivityDate = Date.today();
            taskObj.WhoId = con.Id;
            taskObj.WhatId = acc.Id;
            taskObj.OwnerId = usr.Id;
            taskList.add(taskObj);
            
            Task taskObj1 = new Task();
            taskObj1.Priority = 'High';
            taskObj1.Status = 'Completed';
            taskObj1.Subject = 'Send Quote';
            taskObj1.ActivityDate = Date.today();
            taskObj1.WhoId = con.Id;
            taskObj1.OwnerId = usr.Id;
            taskList.add(taskObj1);
            
            Task taskObj2 = new Task();
            taskObj2.Priority = 'Normal';
            taskObj2.Status = 'In Progress';
            taskObj2.Subject = 'Send Letter';
            taskObj2.ActivityDate = Date.today();
            taskObj2.OwnerId = usr.Id;
            taskList.add(taskObj2);
            
            insert taskList;
            System.assertEquals(3, taskList.size());
        }
    }
    
    @isTest
    private static void getDataTest(){
        User u = [SELECT Id FROM User Where Alias = 'tUOne'];
        System.runAs (u) {
            List<FS_TaskSchemaClass> resultList = recentActivitiesController.getDataList();
            System.assertEquals(1, resultList.size()); //1 because 2nd has status as 'Completed'
            
            
        }
    }
    
    
        @isTest
    private static void getDataTestOne(){
        User u = [SELECT Id FROM User Where Alias = 'tUOne'];
        Task task=[select id,WhoId from Task where Status='Completed' limit 1];
        task.WhoId=null;
        update task;
        System.runAs (u) {
            List<FS_TaskSchemaClass> resultList = recentActivitiesController.getDataList();
            System.assertEquals(1, resultList.size()); //1 because 2nd has status as 'Completed'
            
            
        }
    }

}