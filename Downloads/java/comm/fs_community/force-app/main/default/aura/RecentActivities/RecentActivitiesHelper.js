({
	doInit :function(component, event, helper){
        var action = component.get('c.getDataList');
        action.setCallback(this,$A.getCallback(function(response){
            var state = response.getState();
            
            if(state=== 'SUCCESS'){
                console.log('init Success');
               
               
                  var result = response.getReturnValue();
                    component.set('v.Tasks',result);
                console.log('tasklist:'+result);             
            }
            else {
                 var error = response.getError();
                console.error(error);
            }
            
        }));
        $A.enqueueAction(action);
    }
})