trigger updateTaskOnClientApplicationTrigger on Client_Application__c (after insert,after update) {
    List<Task> tasklist=new list<Task>();
    List<Task> tasklistt=new list<Task>();
    List<Task> taskLists=new List<Task>(); 
  List<Client_Application__c> capps=[Select Id,Document_Status__c,Name from Client_Application__c where Id=:Trigger.new];
  //List<Client_Application__c> capplications=Trigger.old;
  Integer flag=0;
  
  ID ownerIs= UserInfo.getUserId();
  if(Trigger.isInsert==true)
  {
      for(Client_Application__c capp:capps)
      {
          if(!capp.Document_Status__c && trigger.isInsert )
          {
              Task newTask=new Task();
              newTask.Subject = CommunityConstants.TASK_SUBJECT_SUBMIT_PENDING_DOCS+''+capp.Name;
              newTask.WhatId = capp.Id;

              newTask.OwnerId = ownerIs;
              newTask.Type = CommunityConstants.TASK_TYPE_EVENT;
              newTask.Priority=CommunityConstants.TASK_PRIORITY_HIGH;
              newTask.Status=CommunityConstants.TASK_STATUS_NOT_STARTED;
              newTask.ActivityDate=System.today()+7;
              newTask.fsCore__Source_Activity_ID__c=capp.Name;
              tasklistt.add(newTask);
          }

          }
  
  }
  if(Trigger.isUpdate==true)
  {
      for(Client_Application__c capp:capps)
      {
       List<Task> tasks = [select id, whatId,Subject,Status from task where WhatId =: capp.Id];

       if(capp.Document_Status__c)
          {
            for(Task tac:tasks)
              {
                  tac.Subject='Documents uploaded for '+''+capp.Name;
                  tac.Status=CommunityConstants.TASK_STATUS_COMPLETED;
                  tac.ActivityDate=System.today();
                  tasklist.add(tac);
                  flag=1;
              }
       }
       }
  }
   for(Client_Application__c capplication:capps)
   {
     List<Client_Document__c> cdocs=[Select Id,Client_Application_Number__c from Client_Document__c where Client_Application_Number__c=:capplication.Name];
     if(cdocs.size()>0 && flag==0)
     {
      List<Task> tasks = [select id, whatId,Subject,Status from task where WhatId =: capplication.Id];

      
            for(Task tacc:tasks)
              {
                  tacc.Subject=CommunityConstants.TASK_SUBJECT_RESUME_DOC_SUBMISSION +''+capplication.Name;
                  tacc.Status=CommunityConstants.TASK_STATUS_IN_PROGRESS;
                  tasklists.add(tacc);
              }
       
       
     }
   
  }
    if(tasklist.size()>0)
    {
        update tasklist;
    }
    if(tasklists.size()>0){
        update tasklists;
    }
    if(tasklistt.size()>0)
    {
        insert tasklistt;
    }
    
}