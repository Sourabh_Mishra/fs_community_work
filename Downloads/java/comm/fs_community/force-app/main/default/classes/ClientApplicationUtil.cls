public without sharing class ClientApplicationUtil {
    @AuraEnabled
    public static Contact getContactRecord(){
        Id UserId = userInfo.getUserId();
        System.debug('UserId  '+UserId);
        //Id UserId= '0055w00000DdfyMAAR';
        List<User> usrList = [SELECT ContactId FROM User Where Id=:UserId];
        List<Contact> con = new List<Contact>();
        if(usrList[0].ContactId!=null){
            Id conId = usrList[0].ContactId;
            con = [SELECT Id,Name,FirstName,LastName,MobilePhone,MailingCity,MailingState,MailingStreet,Birthdate,Email FROM Contact WHERE Id=:conId];
            System.debug(con);
        }
        else{
            con = [SELECT Id,Name,FirstName,LastName,MobilePhone,MailingCity,MailingState,MailingStreet,Birthdate,Email FROM Contact WHERE OwnerId=:UserId ORDER BY CreatedDate DESC LIMIT 1];
        }
        if(con.size()>0){ return con[0]; }
        
        else
        {
            Contact newCon = new Contact();
            return newCon;
        }
    }
    @AuraEnabled
    public static void updateContact(Contact pCon){
        Id UserId = userInfo.getUserId();
        Id conId = [SELECT ContactId FROM User Where Id=:UserId].ContactId;
        Contact con = [SELECT Id,Name,FirstName,LastName,MobilePhone,MailingCity,MailingState,MailingStreet,Birthdate,Email FROM Contact WHERE Id=:conId];
        con.FirstName = pCon.firstName;
        con.LastName = pCon.lastName;
        con.Birthdate = pCon.Birthdate;
        con.Email = pCon.Email;
        con.MobilePhone = pCon.MobilePhone;
        con.MailingState = pCon.MailingState;
        con.MailingCity = pCon.MailingCity;
        con.MailingStreet = pCon.MailingStreet;
        update con;
    }
    @AuraEnabled
    public static Id getEmploymentRecId(){
        Id UserId = userInfo.getUserId();
        Id activeRecordTypeId = Schema.getGlobalDescribe().get(CommunityConstants.ENTITY_CLIENT_EMPLOYMENT).getDescribe().getRecordTypeInfosByName().get(CommunityConstants.ACTIVE).getRecordTypeId();
        List<Client_Employment__c> employmentObj = [SELECT Id FROM Client_Employment__c WHERE OwnerId=:UserId AND RecordTypeId=:activeRecordTypeId ORDER BY CreatedDate DESC LIMIT 1];
        if(employmentObj.size()!=0){
            Id employmentId = employmentObj[0].Id;
            return employmentId;
        }
        return null;
    }
    @AuraEnabled
    public static Id getFinancialRecId(){
        Id UserId = userInfo.getUserId();
        Client_Financial__c financial = [select id, Name from Client_Financial__c where OwnerId=:UserId LIMIT 1];
        return financial.id;
    }
      @AuraEnabled
    public static List<Client_Financial__c> getIncomeRecords(){
        RecordType recordTypeObj = [Select Id, Name From RecordType Where SobjectType = :CommunityConstants.ENTITY_CLIENT_FINANCIAL AND Name = :CommunityConstants.INCOME];
        System.debug(recordTypeObj);
        Id UserId = userInfo.getUserId();
        List<Client_Financial__c> financials = new List<Client_Financial__c>();
        financials = [SELECT Id
                      ,Name
                      ,Client_Financial_Family__c 
                      ,Client_Financial_Type__c 
                      ,Frequency__c 
                      ,Is_Non_Taxable__c 
                      ,Stated_Amount__c 
                      ,Monthly_Stated_Amount__c
                      ,RecordTypeId
                      FROM Client_Financial__c WHERE OwnerId=:UserId AND RecordTypeId = :recordTypeObj.Id ORDER BY CreatedDate DESC];
        return financials;
    }
    @AuraEnabled
    public static List<Client_Financial__c> getLiabilityRecords(){
        RecordType recordTypeObj = [Select Id, Name From RecordType Where SobjectType = :CommunityConstants.ENTITY_CLIENT_FINANCIAL AND Name = :CommunityConstants.LIABILITY];
        Id UserId = userInfo.getUserId();
        
        List<Client_Financial__c> financials = new List<Client_Financial__c>();
        financials = [SELECT Id
                      ,Name
                      ,Client_Financial_Family__c 
                      ,Client_Financial_Type__c 
                      ,Frequency__c 
                      ,Is_Non_Taxable__c 
                      ,Stated_Amount__c 
                      ,Monthly_Stated_Amount__c
                      ,RecordTypeId
                      FROM Client_Financial__c WHERE OwnerId=:UserId AND RecordTypeId = :recordTypeObj.Id ORDER BY CreatedDate DESC];
        return financials;
    }
    @AuraEnabled
    public static Id getLiabilityRecTypeId(){
     Map<String, Schema.RecordTypeInfo> financialsRecTypeMap = Schema.SObjectType.Client_Financial__c.getRecordTypeInfosByDeveloperName();
        Id recordTypeId = financialsRecTypeMap.get(CommunityConstants.LIABILITY).getRecordTypeId();
        return recordTypeId;
    }
     @AuraEnabled
    public static Id getIncomeRecTypeId(){
     Map<String, Schema.RecordTypeInfo> financialsRecTypeMap = Schema.SObjectType.Client_Financial__c.getRecordTypeInfosByDeveloperName();
        Id recordTypeId = financialsRecTypeMap.get(CommunityConstants.INCOME).getRecordTypeId();
        return recordTypeId;
    }
    

    @AuraEnabled
    public static Client_Application__c getApplicationRecId(){
        Id UserId = userInfo.getUserId();
        List<Client_Application__c> applicationObj = [SELECT Id,Name FROM Client_Application__c WHERE OwnerId=:UserId ORDER BY CreatedDate DESC LIMIT 1];
        if(applicationObj.size()!=0){
            Id applicationId = applicationObj[0].Id;
            //return applicationId;
           return applicationObj[0];
        }
        return null;
    }
    
   
    // For another component
    @AuraEnabled
    public static User getCurrUser(){
        Id currUserId = UserInfo.getUserId();
        User currUser = [SELECT Id,Name,Alias,ProfileId FROM User WHERE Id=:currUserId];
        System.debug('Id of logged in User: '+currUserId);
        System.debug('Logged in user Name: '+currUser.Name);
        System.debug('Logged in user Alias: '+currUser.Alias);
        return currUser;
    }
    @AuraEnabled
    public static List<Client_Application__c> getApplications(){
        Id UserId = userInfo.getUserId();
        List<Client_Application__c> appList = [SELECT Name,Product__c,Purpose__c,Loan_Amount__c,Document_Status__c, Application_Status__c, Lending_Application__c,
                                               Lending_Application__r.Name FROM Client_Application__c WHERE OwnerId=:userId ORDER BY CreatedDate DESC];
        System.debug('Size'+appList.size());
        System.debug('List'+appList);
        return appList;
    }
    public static Date getPaymentStartDate(Date pLoanStartDate, String pPaymentCycle){
        Date paymentStartDate;
        if(pLoanStartDate != null && pPaymentCycle != null){
            paymentStartDate = fsCore.DateUtilGlobal.adjustDateByBillingCycle(pLoanStartDate
                                                                              , pLoanStartDate.day()
                                                                              , fsCore.Constants.DUMMY_SECOND_DUE_DAY
                                                                              , pPaymentCycle
                                                                              , 1);
        }
        System.debug(fsCore.Constants.DUMMY_SECOND_DUE_DAY + ' '+paymentStartDate);
        return paymentStartDate;
    }
  
    
    public static List<ContentVersion> getContentVersion(Id conId)
    {
        List<ContentDocumentLink> conDocList = [select ContentDocumentId, LinkedEntityId from ContentDocumentLink 
                                                where LinkedEntityId =:conId];
        System.debug(conDocList);
        List<Id> contentDocIds = new List<Id>();
        for(ContentDocumentLink contentDocLink : conDocList) {
            contentDocIds.add(contentDocLink.ContentDocumentId);
        }
        
        List<ContentVersion> contentVersions = [SELECT ContentDocumentId, Title, FileType, ContentSize, FileExtension, ContentUrl,
                                                ContentModifiedDate FROM ContentVersion WHERE ContentDocumentId IN :contentDocIds order by createdDate DESC Limit 6 ];
        System.debug(contentVersions);
        return contentVersions;
    }
    
    @AuraEnabled
    public static boolean ifStatusIsSubmitted(String appNum)
    {
          List<Client_Document__c>  cdocs=[Select Id,Type__c,Status__c from Client_Document__c where Client_Application_Number__c=: appNum ];
          integer flag=0;
          for(Client_Document__c cd:cdocs)
          {
              if(cd.Status__c==CommunityConstants.SUBMITTED)
              {
                  flag=1;
              }
             
          }
        if(flag==1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
   
    
    @AuraEnabled  
    public static void deleteFile(String contentDocumentId){ 
        delete [SELECT Id from ContentDocument WHERE Id = :contentDocumentId];       
    } 
    
    @AuraEnabled
    public static boolean addAllClear(String appNum)
    {
        Client_Application__c app = [SELECT Product__c FROM Client_Application__c WHERE Name = :appNum];
        String LoanType=app.Product__c;
        List<String> pick=new List<String>();
        List<Client_Document__c> cdocs=[select id from Client_Document__c  where Client_Application_Number__c=: appNum and Status__c=:CommunityConstants.SUBMITTED];
        List<Document_Metadata__mdt> clientAppMappings = [select Product__c,Documents_Available__c FROM Document_Metadata__mdt where Is_Active__c=true];
        System.debug(clientAppMappings);   
        for(Document_Metadata__mdt mapping:clientAppMappings)
        {
            if(LoanType==mapping.Product__c)
            {
                pick.add(mapping.Documents_Available__c);
            }
        }
        if(pick.size()==cdocs.size())
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    
    @AuraEnabled  
    public static boolean isSubmitted(String appNum){ 
         List<Client_Document__c>  cdocs=[Select Status__c,id from Client_Document__c where Client_Application_Number__c=: appNum ];
         List<Client_Application__c> capps=[Select id from Client_Application__c  ];
         if(cdocs.size()==0 || capps.size()==1)
         {
             return false;
         }
        else
        {
            return true;
        }
    } 
    
    
    
   /* @AuraEnabled
    public static boolean areAllSubmitted(String appNum)
    {
        List<Client_Document__c> cdocs=[Select Status__c from Client_Document__c where Client_Application_Number__c=: appNum];
        integer flag=0;
        for(Client_Document__c cd:cdocs)
        {
            if(cd.Status__c!='Submitted')
            {
               flag=1; 
            }
           
        }
        if(flag==1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }*/
    
    @AuraEnabled
    public static List<String> availableDocs(String appNum)
    {
        Client_Application__c app = [SELECT Product__c FROM Client_Application__c WHERE Name = :appNum];
        String LoanType=app.Product__c;
        System.debug('Loan Type is:'+LoanType);
        List<String> pick=new List<String>();
        List<Document_Metadata__mdt> clientAppMappings = [select Product__c,Documents_Available__c FROM Document_Metadata__mdt where Is_Active__c=true];
        System.debug(clientAppMappings);   
        for(Document_Metadata__mdt mapping:clientAppMappings)
        {
            if(LoanType==mapping.Product__c)
            {
                pick.add(mapping.Documents_Available__c);
            }
        }
        System.debug('Available documents for this Loan Application:'+pick);
        
        
        List<Client_Document__c>  cdocs=[Select Type__c from Client_Document__c where Client_Application_Number__c=: appNum];
        List<String> submitteddocs=new List<String>();
        for(Client_Document__c cdoc:cdocs)
        {
            submitteddocs.add(cdoc.Type__c);  
        }
        List<String> filteredDocs=new List<String>();
        if(submitteddocs!=null && !submitteddocs.isEmpty())
        {
            for(String pc:pick)
            {
                if(!submitteddocs.contains(pc))
                {
                    filteredDocs.add(pc);
                }
            }
            System.debug('newdocs:'+filteredDocs);
            return filteredDocs;
            
        }
        else{
            return pick;
        }
        
    }
    
    /*@AuraEnabled
      public static List<String> availableFileTypes(String conString)
      {
      System.debug('String is:'+conString);
      String correctString=conString.replaceAll(' ','');
      String cString=correctString.toLowercase();
      List<String> res = cString.split(',');
      System.debug('SPLIT LIST IS:'+res);
      return res;
      }*/
    
    @AuraEnabled
    public static Id getClientApplication(String appNum)
    {
        Client_Application__c app = [SELECT Id,Primary_Customer_Contact__c FROM Client_Application__c WHERE Name = :appNum];
        Id linkedRecId = app.Id;
        System.debug(linkedRecId);
        return linkedRecId;
    }
    
    @AuraEnabled
    public static void deleteAllDocuments(String appNum)
    {
        
       List<Client_Document__c> cdocs=[Select Id from Client_Document__c where Client_Application_Number__c=:appNum and Status__c=:CommunityConstants.SUBMITTED];
             
       delete cdocs;
    }
    
     @AuraEnabled
    public static void deleteFilesRecord(String appNum){
        
        List<Client_Document__c>  cdocs=[Select Id,Type__c from Client_Document__c where Client_Application_Number__c=: appNum];
        Map<String,List<ContentVersion>> mapObj=new Map<String,List<ContentVersion>>();
        String deletedType=null;
        for(Client_Document__c cdoc:cdocs)
        {
            List<ContentVersion> cons=getContentVersion(cdoc.id);
            if(cons.size()>0)
            {
                mapObj.put(cdoc.Type__c,cons);
            }
            else
            {
                deletedType=cdoc.Type__c;  
            }
        }

        
        if(deletedType!=null)
        {
            Client_Document__c cdoc=[Select Id from Client_Document__c where Client_Application_Number__c=: appNum AND  Type__c=: deletedType];
            Delete cdoc;
        }
        
    }
    
    
    
    @AuraEnabled
    public static boolean documentStatus(String appNum)
    {
        Client_Application__c app = [SELECT Id,Product__c FROM Client_Application__c WHERE Name = :appNum];
        Id docId=app.id;
        System.debug('docid is:'+docId);
        String LoanType=app.Product__c;
        System.debug('Loan Type is:'+LoanType);
        List<String> pick=new List<String>();
        List<Document_Metadata__mdt> clientAppMappings = [select Product__c,Documents_Available__c FROM Document_Metadata__mdt where Is_Active__c=true];
        System.debug(clientAppMappings);   
        for(Document_Metadata__mdt mapping:clientAppMappings)
        {
            if(LoanType==mapping.Product__c)
            {
                pick.add(mapping.Documents_Available__c);
            }
        }
        integer DocsNeeded=pick.size();
        System.debug('Number of documents need to be submitted for this loan application'+DocsNeeded);
        
        List<Client_Document__c>  cdocs=[Select Id,Type__c,Status__c from Client_Document__c where Client_Application_Number__c=: appNum AND (Status__c=:CommunityConstants.SUBMITTED OR Status__c=:CommunityConstants.APPROVED)];
        System.debug(cdocs);
        integer DocSubmitted=cdocs.size();
        System.debug('Number of documents submitted for this loan application'+DocSubmitted);
        
        if(DocSubmitted>=DocsNeeded)
        {
            System.debug('All documents submitted for this application');
            Client_Application__c DocStatus=[Select Document_Status__c from Client_Application__c where Name = :appNum ];
            DocStatus.Document_Status__c = true;
            
            update DocStatus;
            return true;
        }
        else
        {
            System.debug('Documents are pending');
            Client_Application__c DocStatus=[Select Document_Status__c from Client_Application__c where Name = :appNum ];
            DocStatus.Document_Status__c = false;
            
            update DocStatus;            
            return false;
        }
        
    }
    
       @AuraEnabled
       public static void attachDocuments(String appNum)
       {
            Client_Application__c capps=[Select Id,Name,Product__c from Client_Application__c where Name=:appNum];
            String LoanType=capps.Product__c;
            String appNumber=appNum;
            System.debug('name is :'+capps.Name);
            List<Document_Metadata__mdt> clientAppMappings = [select Product__c,Documents_Available__c FROM Document_Metadata__mdt where Is_Active__c=true];
            System.debug(clientAppMappings);   
            for(Document_Metadata__mdt mapping:clientAppMappings)
            {
            if(LoanType==mapping.Product__c)
            {
               prepopulateDocuments(mapping.Documents_Available__c,appNumber);
            }
        }
       }

       public static void prepopulateDocuments(String docType,String appNum)
       {
           System.debug('docType is:'+docType);
           List<Client_Document__c> cdocu=[Select Id,Name from Client_Document__c where Type__c=:docType and Client_Application_Number__c=:appNum];
           System.debug('cdocu is'+cdocu);
            Client_Application__c app = [SELECT Id,Product__c FROM Client_Application__c WHERE Name = :appNum];
            Id linkedRec = app.Id;
            Client_Document__c cdocs=[Select Name,Id from Client_Document__c where Type__c=:docType  order by createddate DESC limit 1];
            System.debug('client docs:'+cdocs.Id);
            System.debug('client docu name is :'+cdocs.Name);
           
           if(cdocs.id!=null  && cdocu.size()==0)
           {
                //where LinkedEntityId =:cdocs.Id limit 1
                ContentDocumentLink conDoc = [select ContentDocumentId, LinkedEntityId from ContentDocumentLink where LinkedEntityId =:cdocs.Id limit 1];
                System.debug('conDoc is:'+conDoc);
                ContentVersion contentVersion = [SELECT id,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId =:conDoc.ContentDocumentId];
                
                Client_Document__c cdoc=new Client_Document__c();
                cdoc.Client_Application_Number__c=appNum;
                cdoc.Status__c=CommunityConstants.SUBMITTED;
                cdoc.Type__c=docType;
                // cdoc.Document_Name__c=getDocSetUp(docType);
                cdoc.Client_Application__c=linkedRec;
                cdoc.Document_Classification__c=getDocClassification(docType);
        
                insert cdoc;
               
                Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:contentVersion.id].ContentDocumentId;
               
                ContentDocumentLink cDocLinks = new ContentDocumentLink();
                cDocLinks.ContentDocumentId = conDocument;
                cDocLinks.LinkedEntityId = cdoc.id;
                cDocLinks.ShareType = 'I';
                cDocLinks.Visibility = CommunityConstants.DOC_VISIBILITY_ALL_USERS;  
                insert cDocLinks;
               }
         }


    @AuraEnabled
    public static ContentDocumentLink saveChunks(Id parentId, String fileName, String base64Data, String contentType, String fileId,String appNum,String docType) {
        
        Client_Application__c app = [SELECT Id,Product__c FROM Client_Application__c WHERE Name = :appNum];
        List<Client_Document__c> cd=[Select Id,Status__c FROM Client_Document__c WHERE Client_Application_Number__c=:appNum and Type__c=:docType and Status__c=:CommunityConstants.REJECTED];
        //ContentDocumentLink conDoc = [select ContentDocumentId, LinkedEntityId from ContentDocumentLink 
                                               // where LinkedEntityId =:cd.id];
        List< ContentVersion> conList=new List<ContentVersion>();                                    
        Id linkedRec = app.Id;
        System.debug(linkedRec);
        System.debug('LINKED RECORD ID IS:'+linkedRec);
        ContentDocumentLink condocLink=null;
        if(cd.size()>0)
        {
        System.debug('In cd.....');
           
        if (fileId == '' || fileId==null) {
        for(Client_Document__c cdocss:cd)
        {
        ContentDocumentLink conDoc = [select ContentDocumentId, LinkedEntityId from ContentDocumentLink 
                                                where LinkedEntityId =:cdocss.id];
        condocLink=conDoc;
        ContentVersion contentVersion = [SELECT id,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId =:conDoc.ContentDocumentId];
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        //contentVersion.PathOnClient = fileName;
        contentVersion.Title = fileName;
        contentVersion.VersionData = EncodingUtil.base64Decode(base64Data);
       // contentVersion.IsMajorVersion = false;
        conList.add(contentVersion);
        fileId=contentVersion.id;
        }
        update conList;
        }
        else {
        appendToFile(fileId, base64Data);
        }
        List<Client_Document__c> updateDoc=new List<Client_Document__c>();
        for(Client_Document__c cdd:cd)
         {
             cdd.Status__c=CommunityConstants.SUBMITTED;
             updateDoc.add(cdd);
         }
        update updateDoc;
        //add return stat
        return condocLink;
        }
        
        else
        {
        if (fileId == '' || fileId==null) {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            appendToFile(fileId, base64Data);
        }
       Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:fileId].ContentDocumentId;
        /*ContentDocumentLink cDocLink = new ContentDocumentLink();
          cDocLink.ContentDocumentId = conDocument;
          cDocLink.LinkedEntityId = parentId ;
          cDocLink.ShareType = 'I';
          cDocLink.Visibility = CommunityConstants.DOC_VISIBILITY_ALL_USERS;
          Insert cDocLink;*/
        
        Client_Document__c cdoc=new Client_Document__c();
        cdoc.Name=docType+'('+appNum+')';
        cdoc.Client_Application_Number__c=appNum;
        cdoc.Status__c=CommunityConstants.SUBMITTED;
        cdoc.Type__c=docType;
        // cdoc.Document_Name__c=getDocSetUp(docType);
        cdoc.Client_Application__c=linkedRec;
        cdoc.Document_Classification__c=getDocClassification(docType);
        
        insert cdoc;
        
        ContentDocumentLink cDocLinks = new ContentDocumentLink();
        cDocLinks.ContentDocumentId = conDocument;
        cDocLinks.LinkedEntityId = cdoc.id;
        cDocLinks.ShareType = 'I';
        cDocLinks.Visibility = CommunityConstants.DOC_VISIBILITY_ALL_USERS;  
        insert cDocLinks;
        
        /*Id userId = userInfo.getUserId();
        System.debug('USER ID IS:'+userId);

        List<Client_Application__c> applicationObj = [SELECT Id FROM Client_Application__c WHERE OwnerId=:userId  ];
        List<ContentDocumentLink> conLink=new List<ContentDocumentLink>();
        if(applicationObj.size()!=0)
        {
        for(Client_Application__c capp:applicationObj)
        {
        ContentDocumentLink cDocLinks = new ContentDocumentLink();
        cDocLinks.ContentDocumentId = conDocument;
        cDocLinks.LinkedEntityId = capp.Id ;
        cDocLinks.ShareType = 'I';
        cDocLinks.Visibility = CommunityConstants.DOC_VISIBILITY_ALL_USERS;  
        conLink.add(cDocLinks);
        }
        }
        Insert conLink; */
        
        return cDocLinks;
        }
        }
    
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; 
        cVersion.PathOnClient = fileName;
        cVersion.Origin = 'H';
        cVersion.Title = fileName;
        cVersion.VersionData = EncodingUtil.base64Decode(base64Data);
        cVersion.IsMajorVersion = false;
        Insert cVersion;
        return cVersion.Id;
    }
    
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        system.debug('appending');
        ContentVersion a = [
            SELECT Id, VersionData,ContentDocumentId
            FROM ContentVersion
            WHERE ContentDocumentId = :fileId
        ];
        String existingBody = EncodingUtil.base64Encode(a.VersionData);
        a.VersionData = EncodingUtil.base64Decode(existingBody + base64Data); 
        update a;
    }
    
    @AuraEnabled
    public static Id userDetails(){
        id userId = UserInfo.getUserId();
        System.debug('USER ID IS:'+userId);
        User u = [select id, contactId from User where id = : userId];
        id getContactId = u.contactId;
        system.debug('RELATED CONTACT IS:' + getContactId);
        return getContactId;
    }
    
    public static String getDocClassification(String cdocType)
    {
      
        
        List<Document_Setup__mdt> cmaps=[Select Document_Type__c,Document_Classification__c from Document_Setup__mdt where Is_Active__c=true ];
        String docClassification=null;
        for(Document_Setup__mdt cmapp:cmaps)
        {
            if(cdocType==cmapp.Document_Type__c)
            {
                docClassification=cmapp.Document_Classification__c;
            }
        }
        System.debug('Doc classification is:'+docClassification);
        return docClassification;
    }
    
    
    
    /*public static id getDocSetUp(String docType)
     {
     fsCore__Document_Setup__c cdocs=new fsCore__Document_Setup__c();
     cdocs.fsCore__Is_Active__c=true;
     cdocs.fsCore__Default_Company_Availability__c='Available';
     cdocs.fsCore__Default_Product_Availability__c='Available';
     cdocs.fsCore__Default_Selection_Rule_Result__c='Available';
     cdocs.fsCore__Document_Classification__c=getDocClassification(docType);
     Integer randomNumber = Integer.valueof((Math.random() * 100));
     cdocs.fsCore__Document_Code__c=docType+randomNumber;
     cdocs.fsCore__Is_Lease__c=true;
     cdocs.fsCore__Selection_Order__c=0;
     cdocs.fsCore__Line_Of_Business__c='Origination';
     insert cdocs;
     return cdocs.id;

    }*/
    
    @AuraEnabled
    public static void deleteFinancial(Id financialId){
        Client_Financial__c financial = [select Id, Name from Client_Financial__c where Id=:financialId];
        delete financial;
    }
    
    
    
}