({
  doInit: function (component, event, helper) {
        console.log('all set');
     },
   
  refreshPage: function(component, event, helper) {
        component.set("v.popupModal", false);
        $A.get('e.force:refreshView').fire();
    },
   
  doSave: function(component, event, helper){
        var documentOption = component.get("v.docClassification");
        console.log('documentOption:'+documentOption);
        console.log('selected item is:'+documentOption);
        component.set("v.selectedDocType",documentOption );
        var fileme=component.get("v.fileTypeList");
        var fileext=component.get("v.fileextension");
        console.log('SELECTED FILE EXTENSION TO UPLOAD:'+fileext);
        if (component.find("fileId").get("v.files").length != 0 && fileme.includes(fileext)) {
            helper.uploadHelper(component, event,helper);
            console.log('saving..');
            
        } 
        else {
            console.log('no file attached');
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please select a valid file',
                type: 'error'
            });
            toastEvent.fire();
        }
    
            helper.getRefresh(component,event,helper);
        },
    
    handleFilesChange: function(component, event, helper) {
       var fileName = '';
       var fileextension='';
       if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
            component.set("v.buttonaction",true);
           }
        console.log('FILE NAME IS:'+fileName);
        fileextension= fileName.substring(fileName.lastIndexOf("."));
        console.log(fileextension);
        component.set("v.fileextension",fileextension);
        var filetypes=component.get("v.fileTypeList");
        console.log('FILETYPES ARE:'+filetypes);
        if(filetypes.includes(fileextension))
        {
        console.log('FILE EXTENSION IS'+fileName.substring(fileName.lastIndexOf(".")));
        component.set("v.fileName", fileName);
        var fileNameChanger = component.find("fileNameChange");
        $A.util.removeClass(fileNameChanger,'slds-text-color_error');
        $A.util.addClass(fileNameChanger,'slds-text-color_success');
        }
        else 
        {
         var toastEvent = $A.get("e.force:showToast");
         toastEvent.setParams({
                        message : 'Please add a file with valid file extension',
                        type : 'error'
                    });
         toastEvent.fire();
         }
        },
     })