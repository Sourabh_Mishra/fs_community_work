public without sharing class ApplicationReviewController {
    @AuraEnabled
    public static Contact getContactFromApplication(){
        return ClientApplicationUtil.getContactRecord();
    }
    @AuraEnabled
    public static Id getEmploymentIdFromApplication(){
        return ClientApplicationUtil.getEmploymentRecId();
    }
   
    @AuraEnabled
    public static List<Client_Financial__c> getIncomeRecordsFromApplication(){
        return ClientApplicationUtil.getIncomeRecords();
    }
    @AuraEnabled
    public static List<Client_Financial__c> getLiabilityRecordsFromApplication(){
        return ClientApplicationUtil.getLiabilityRecords();
    }
    @AuraEnabled
    public static Id getIncomeRecTypeId(){
        return ClientApplicationUtil.getIncomeRecTypeId();
    }
    @AuraEnabled
    public static Id getLiabilityRecTypeId(){
        return ClientApplicationUtil.getLiabilityRecTypeId();
    }
    @AuraEnabled
    public static Id getApplicationIdFromName(String appNum){
        return ClientApplicationUtil.getClientApplication(appNum);
    }
    
    @AuraEnabled
    public static Map<String,List<documentStatus>> getAlreadyUploadedDocuments (String appNum){
        
        List<Client_Document__c>  cdocs=[Select Id,Type__c,Status__c from Client_Document__c where Client_Application_Number__c=: appNum];
        Map<String,List<documentStatus>> mapObj=new Map<String,List<documentStatus>>();
        
        for(Client_Document__c cdoc:cdocs)
        {
            List<documentStatus> docStatusList=new List<documentStatus>();
            documentStatus wc = new documentStatus();
            wc.conVersion =getContentVersion(cdoc.id) ;
            wc.docStatus = cdoc.Status__c;
            docStatusList.add(wc);  
            mapObj.put(cdoc.Type__c,docStatusList);
        }
        System.debug(mapObj);
        
        return mapObj;
    }
    
    @AuraEnabled  
    public static void deleteFile(String contentDocumentId){ 
        ClientApplicationUtil.deleteFile(contentDocumentId);       
    }  
    @AuraEnabled
    public static void updateContact(Contact pCon){
        ClientApplicationUtil.updateContact(pCon);
    }
    @AuraEnabled
    public static List<String> fetchAvailableDocs(String appNum)
    {
        return ClientApplicationUtil.availableDocs(appNum);
    }
    
    /* @AuraEnabled
public static List<String> getFileTypes(String conString)
{
return ClientApplicationUtil.availableFileTypes(conString);
}*/
    
     @AuraEnabled
    public static boolean isSubmitted(String appNum)
    {
        return ClientApplicationUtil.isSubmitted(appNum);
    }
    
    @AuraEnabled
    public static ContentDocumentLink saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId, String appNum,String docType)
    {
        return ClientApplicationUtil.saveChunks(parentId,fileName,base64Data,contentType,fileId,appNum,docType);
        
    }
    
    @AuraEnabled
    public static Id getClientApplications (String appNum)
    {
        return ClientApplicationUtil.getClientApplication(appNum);
    }
    
    @AuraEnabled
    public static boolean documentStatus(String appNum)
    {
        return ClientApplicationUtil.documentStatus(appNum);
    }
    
   /* @AuraEnabled
    public static boolean areAllSubmitted(String appNum)
    {
        return ClientApplicationUtil.areAllSubmitted(appNum);
    }*/
    
    @AuraEnabled
    public static void deleteFilesRecord(String appNum)
    {
        ClientApplicationUtil.deleteFilesRecord(appNum);
    }
     
    @AuraEnabled
    public static void deleteAllDocuments(String appNum)
    {
        ClientApplicationUtil.deleteAllDocuments(appNum);
    }
    
     @AuraEnabled
    public static boolean ifStatusIsSubmitted(String appNum)
    {
        return ClientApplicationUtil.ifStatusIsSubmitted(appNum);
    }
    

     @AuraEnabled
    public static void attachDocuments(String appNum)
    {
        ClientApplicationUtil.attachDocuments(appNum);
    }
     
     @AuraEnabled
    public static boolean addAllClear(String appNum)
    {
        return  ClientApplicationUtil.addAllClear(appNum);
    }

     
    public static List<ContentVersion> getContentVersion(Id conId)
    {
        List<ContentDocumentLink> conDocList = [select ContentDocumentId, LinkedEntityId from ContentDocumentLink 
                                                where LinkedEntityId =:conId];
        System.debug(conDocList);
        List<Id> contentDocIds = new List<Id>();
        for(ContentDocumentLink contentDocLink : conDocList) {
            contentDocIds.add(contentDocLink.ContentDocumentId);
        }
        
        List<ContentVersion> contentVersions = [SELECT ContentDocumentId, Title, FileType, ContentSize, FileExtension, ContentUrl,
                                                ContentModifiedDate FROM ContentVersion WHERE ContentDocumentId IN :contentDocIds order by createdDate DESC Limit 6 ];
        System.debug(contentVersions);
        return contentVersions;
    }
    
    
    public class documentStatus {
    @AuraEnabled public List<ContentVersion> conVersion                 {get; set;}
    @AuraEnabled public String docStatus                                {get; set;}
    
     public documentStatus(){
        conVersion = new List<ContentVersion>();
        docStatus = null;
     }
}
    
    
  
}