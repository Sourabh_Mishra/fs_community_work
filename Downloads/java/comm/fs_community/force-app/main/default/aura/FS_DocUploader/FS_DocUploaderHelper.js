({
    MAX_FILE_SIZE: 10000000,
    CHUNK_SIZE: 75000,
    

        
       
        uploadHelper: function(component, event, helper) {
        component.set("v.showLoadingSpinner", true);
        component.set("v.uploadStatus", "Uploading...");
        var fileInput = component.find("fileId").get("v.files");
        var file = fileInput[0];
        var self = this;
        if (file.size > self.MAX_FILE_SIZE) {
            component.set("v.showLoadingSpinner", false);
            component.set("v.uploadStatus", "");
        var fileNameChanger = component.find("fileNameChange");
        $A.util.removeClass(fileNameChanger,'slds-text-color_success');
        $A.util.addClass(fileNameChanger,'slds-text-color_error');
            component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
            var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        message : 'File is too large',
                        type : 'error'
                    });
                    toastEvent.fire();
            $A.get('e.force:refreshView').fire();
            return;
        }
        var objFileReader = new FileReader();
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;
            fileContents = fileContents.substring(dataStart);
            self.uploadProcess(component, file, fileContents);
        });
        objFileReader.readAsDataURL(file);
      },
    
    uploadProcess: function(component, file, fileContents) {
        var startPosition = 0;
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
    },
    
   uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId ){
       var cmp=component.get("v.applicationNumbers");
       console.log('application number is:'+cmp);
        var cmps=component.get("v.selectedDocType");
       console.log('doc selected is:'+cmps);
        var getchunk = fileContents.substring(startPosition, endPosition);
        var action = component.get("c.saveChunk");
        console.log('upload RecordId:-->'+component.get("v.recId"));
        console.log('upload file Name:-->'+file.name);
        console.log('upload base64Data:-->'+encodeURIComponent(getchunk));
        console.log('upload contentType:-->'+file.type);
        console.log('upload fileId:-->'+attachId);
        action.setParams({
            parentId: component.get("v.recId"),
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId,
            appNum: component.get("v.applicationNumbers"),
            docType: component.get("v.docClassification")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state:-->'+state);
            if (state == "SUCCESS") {
                
                attachId = response.getReturnValue();
               console.log('attachId -->' + attachId);
                    component.set("v.showLoadingSpinner", false);
                   // component.set("v.uploadStatus","Your file has been uploaded successfully!");
                    var uploadStatusChange = component.find("uploadStatus");
                    $A.util.removeClass(uploadStatusChange,'slds-text-color_error');
                    $A.util.addClass(uploadStatusChange,'slds-text-color_success');
                    component.set("v.showPreview", "true");
                    component.set("v.popupModal", "true");
                    component.set("v.fileUploadedId",attachId);
                    console.log("UploadedFileId -->"+attachId);
                    component.set("v.buttonClicked",false);
                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        message: 'File Uploaded Successfully!',
                        type: 'success'
                    });
                        var cmpEvent = component.getEvent("cmpEvent");
                    cmpEvent.setParams({
                     "message" : "Event Fired" });
                    cmpEvent.fire();
       console.log('After Event');  
                    
                    
                  }
            else if (state == "INCOMPLETE") {
                    console.log('error');
            } 
                else if (state == "ERROR") {
                var errors = response.getError();
                  if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } 
                    else {
                        console.log("Unknown error");
                    }
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        message: 'File could not be Uploaded',
                        type: 'error'
                    });
                    toastEvent.fire();
                    component.set("v.showLoadingSpinner", false);
               component.set("v.uploadStatus", "File could not be uploaded.");
                }
              //location.reload();
           // $A.get('e.force:refreshView').fire(); 
                      

        });
        $A.enqueueAction(action);
    },
    
    getRefresh: function (component, event, helper) {
        component.set("v.fileName"," ");
        component.set("v.uploadStatus"," ");
        
    }    
})