({
	doInit :function(component, event, helper){
        var action = component.get('c.getData');
        action.setCallback(this,$A.getCallback(function(response){
            var state = response.getState();
            var tableHeading = $A.get("$Label.c.Tasks_Title");
            if(state=== 'SUCCESS'){
                console.log('init Success');
                var size = response.getReturnValue().length;
                 component.set('v.size',size);
                if(size>4){
                    component.set('v.processedDataList',response.getReturnValue().slice(0,3));
                    component.set('v.title',tableHeading+' (4+)');
                    component.set('v.showViewMore',true);
                    console.log('if Block, Processed List: '+response.getReturnValue().slice(0,3));
                    console.log('List Size: '+size);
                }
                else{
                    component.set('v.processedDataList',response.getReturnValue());
                    component.set('v.title',tableHeading+' ('+size+')');
                    component.set('v.showViewMore',false);
                    console.log('else Block, Processed List: '+response.getReturnValue());
                    console.log('List Size: '+size);
                }
            }
            else {
                 var error = response.getError();
                console.error(error);
            }
            
        }));
        $A.enqueueAction(action);
    },
     executeRowAction : function (component, event, helper) {
        console.log('review clicked');
        var appNum = event.currentTarget.getAttribute("data-target");
        let navService = component.find("navService");
        let pageReference = {
            type: "comm__namedPage",
            attributes: {
                pageName: 'applicationreview'
            },
            state: {  
                appNumber: appNum
            }  
        };
        sessionStorage.setItem('pageTransfer', JSON.stringify(pageReference.state));
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            component.set("v.url", url ? url : defaultUrl);
            console.log('Success, URL: '+url);
        }), $A.getCallback(function(error) {
            component.set("v.url", defaultUrl);
            console.log('Error Occured, URL: '+url);
        }));        
    },
    navToPage : function (component, event, helper) {
        let navService = component.find("navService");
        let pageReference = {
            type: "comm__namedPage",
            attributes: {
                pageName: 'taskviewmore'
            } 
        };
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            component.set("v.url", url ? url : defaultUrl);
            console.log('view more navigation Success, URL: '+url);
        }), $A.getCallback(function(error) {
            component.set("v.url", defaultUrl);
            console.log('Error Occured in view more navigation, URL: '+url);
        }));       
    }
})