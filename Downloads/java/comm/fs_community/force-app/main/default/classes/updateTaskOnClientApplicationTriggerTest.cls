@isTest
public class updateTaskOnClientApplicationTriggerTest {
    @testSetup
    private static void createTestData(){
     
        TestDataHelper.createLendingAppTestData();
            
        }
  
    @isTest
    private static void testTrigger(){
        
        Client_Application__c clientApp = [select Id, Name
                                          ,Loan_Amount__c
                                          FROM Client_Application__c where Purpose__c =  'Other' limit 1];
        User u = [SELECT Id,ContactId FROM User WHERE Alias='tUOne' LIMIT 1];
       
        System.runAs(u){
            
         List<Task> tasks = [select id, whatId,Subject,Status from task where WhatId =: clientApp.Id];
         System.assert(tasks!=null);
        }
    }
    
    
     @isTest
    private static void testTrigger1(){
        
        Client_Application__c clientApp=[Select Id from Client_Application__c where Purpose__c = 'Other' limit 1 ];
       
        User u = [SELECT Id,ContactId FROM User WHERE Alias='tUOne' LIMIT 1];
        clientApp.Document_Status__c=true;
        update   clientApp;
        System.runAs(u){
         
         List<Task> tasks = [select id, whatId,Subject,Status from task where WhatId =: clientApp.Id];
         System.assert(tasks!=null);
            
         
           
        }
    }

    
    
         @isTest
    private static void testTrigger2(){
        
        Client_Application__c clientApp=[Select Id,Name from Client_Application__c where Purpose__c = 'Other' limit 1];
        Client_Document__c cdocus=new Client_Document__c();
        //cdocus.Name=testClientApp.Id;
        cdocus.Name='Bank Statement'+'('+clientApp.Name+')';
        cdocus.Client_Application_Number__c=clientApp.Name;
        cdocus.Status__c='Submitted';
        cdocus.Type__c='Bank Statement';
        cdocus.Document_Classification__c='Bank Statement';
        insert cdocus;
        
        User u = [SELECT Id,ContactId FROM User WHERE Alias='tUOne' LIMIT 1];
        clientApp.Document_Status__c=false;
        integer flag=0;
        update   clientApp;
        System.runAs(u){
         
        List<Task> tasks = [select id, whatId,Subject,Status from task where WhatId =: clientApp.Id];
        System.assert(tasks!=null);
        }
    }

}