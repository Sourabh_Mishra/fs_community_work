({
    MAX_FILE_SIZE: 10000000,
    CHUNK_SIZE: 75000,
    
    doInit: function(component,event,helper){
      
        var action1=component.get("c.deleteFilesRecord");
        action1.setParams({
            appNum :  component.get('v.applicationNumbers')    
        })
         action1.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('deleteFilesRecord-->'+state);
                    if(state == "SUCCESS") {
                        var result = response.getReturnValue();
                        console.log('deleteFilesRecord -->'+result);
                         console.log('application is:'+ component.get('v.applicationNumbers'));
                     }
                else{
                var error = response;
                console.log('Error in deleteFilesRecord'+error);
            }
        });
        
        var action2=component.get("c.getAlreadyUploadedDocuments");
        helper.showSpinner(component,event);
        action2.setParams({
            appNum : component.get('v.applicationNumbers')    
        })
         action2.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('getDocumentsActionState-->'+state);
                    if(state == "SUCCESS") {
                         var result = response.getReturnValue();
                         console.log('getDocumentsActionResult -->'+result);
                         var arrayMapKeys = [];
                         for(var key in result){
                          //   console.log('resultkey:'+JSON.parse(result[key]));
                         arrayMapKeys.push({key: key, value: result[key]});
                         }
                        console.log('arraymapkeys:'+arrayMapKeys);
                        component.set("v.mapValues", arrayMapKeys);
                        console.log('array size is:'+arrayMapKeys.length);
                        component.set("v.noOfDocs",arrayMapKeys.length);
                        component.set("v.hasDoc",true);
                        console.log('map is:'+component.get("v.mapValues"));
                        console.log('noofdocs:'+component.get("v.noOfDocs"));
                         }
             helper.hideSpinner(component,event);
        });
        
        var action3=component.get("c.fetchAvailableDocs");
        var options=[];
        action3.setParams({
            appNum : component.get('v.applicationNumbers')    
        })
        action3.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('availableDocs -->'+result);

                component.set('v.documentOptions',result);
            }
        });
        
        var action4 = component.get("c.getClientApplications");
        action4.setParams({
            appNum :  component.get('v.applicationNumbers')    
        })
        action4.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                var result=response.getReturnValue();
                console.log('Client Application Id:'+result);
                component.set("v.recId",result);
            }
            else{
                var error = response;
                console.log('Error in getClientApplications'+error);
            }
        });
        
        var action5 = component.get("c.documentStatus");
        action5.setParams({
            appNum :  component.get('v.applicationNumbers')    
        })
        action5.setCallback(this, function(response){
         var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                var result=response.getReturnValue();
                console.log('Are all documents submitted:'+result);
                
                component.set("v.docStatus",result);
            }
            else{
                var error = response;
                console.log('Error in getClientApplications'+error);
            }
        });
        
     
        var action6 = component.get("c.isSubmitted");
        action6.setParams({
            appNum :  component.get('v.applicationNumbers')    
        })
        action6.setCallback(this, function(response){
         var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                var result=response.getReturnValue();
                console.log('has submitted status for any document: '+result);
                
                component.set("v.hasSubmittedDocs",result);
                console.log('subimitted docs are:'+result);
            }
            else{
                var error = response;
                console.log('Error in hasSubmittedDocs'+error);
            }
        });
        
        var action7 = component.get("c.ifStatusIsSubmitted");
        action7.setParams({
            appNum :  component.get('v.applicationNumbers')    
        })
        action7.setCallback(this, function(response){
         var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                var result=response.getReturnValue();
                console.log('has submitted status for any document: '+result);
                
                component.set("v.ifStatusSubmitted",result);
                console.log('subimitted docs are:'+result);
            }
            else{
                var error = response;
                console.log('Error in hasSubmittedDocs'+error);
            }
        });
        
        var action8 = component.get("c.addAllClear");
        action8.setParams({
            appNum :  component.get('v.applicationNumbers')    
        })
        action8.setCallback(this, function(response){
         var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                var result=response.getReturnValue();
                console.log('has submitted status for any document: '+result);
                
                component.set("v.addAllClear",result);
                console.log('addAllClear docs are:'+result);
            }
            else{
                var error = response;
                console.log('Error in addAllClear'+error);
            }
        });
        $A.enqueueAction(action1);
        $A.enqueueAction(action2);
        $A.enqueueAction(action3);
        $A.enqueueAction(action4); 
        $A.enqueueAction(action5);
        $A.enqueueAction(action6);
        $A.enqueueAction(action7);
        $A.enqueueAction(action8);
    },
    
	deleteUploadedFile : function(component, event) {  
        var action = component.get("c.deleteFile"); 
        var selectedId=component.get("v.selectedId");
        console.log('SELECTED ID IS:'+selectedId);
        action.setParams({
            "contentDocumentId": selectedId           
        });  
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){  
                //this.doInit(component,event,helper);
                component.set("v.showSpinner", false); 
                // show toast on file deleted successfully
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": "File has been deleted successfully!",
                    "type": "success",
                    "duration" : 1000
                });
                toastEvent.fire();
                
            }
            
             $A.get('e.force:refreshView').fire(); 
        });  
        $A.enqueueAction(action);  
    },
    
        attachDocument : function(component, event ,helper) {  
        var action = component.get("c.attachDocuments"); 
        console.log('I am here');
        console.log('capp:'+component.get('v.applicationNumbers'));
        action.setParams({
            appNum : component.get('v.applicationNumbers')          
        });  
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": "Your all files are attached!",
                    "type": "success",
                    "duration" : 1000
                });
                toastEvent.fire();
                
            }
             else{
              
                 var errors = response.getError();
                 if (errors) {
                 if (errors[0] && errors[0].message) {
                     console.log('Error is:'+errors[0].message );//Fetching Custom Message.
        }
                 }}
           //  $A.get('e.force:refreshView').fire(); 
        });  
        $A.enqueueAction(action);  
    },
    
        deleteAllDocuments : function(component, event ,helper) {  
        var action = component.get("c.deleteAllDocuments"); 
        console.log('I am here');
        console.log('capp:'+component.get('v.applicationNumbers'));
        action.setParams({
            appNum : component.get('v.applicationNumbers')          
        });  
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": "Your all files are deleted!",
                    "type": "success",
                    "duration" : 1000
                });
                toastEvent.fire();
                
            }
             else{
              
                 var errors = response.getError();
                 if (errors) {
                 if (errors[0] && errors[0].message) {
                     console.log('Error is:'+errors[0].message );//Fetching Custom Message.
        }
                 }}
           //  $A.get('e.force:refreshView').fire(); 
        });  
        $A.enqueueAction(action);  
    },

    showSpinner : function(component, event){
        var spinner = component.find("waitSpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner : function(component, event){
        var spinner = component.find("waitSpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
       
})