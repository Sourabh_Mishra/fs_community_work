({
    handleClose : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    handleCreate : function(component, event, helper) {
        var spinner = component.find("waitSpinner");
        $A.util.removeClass(spinner, "slds-hide");
        var recId = component.get("v.recordId");
        console.log("recordId --> "+ recId);
        console.log("HandleCreate");
        var action = component.get("c.convertClientApp");
        action.setParams({
            "clientAppId" : recId
        });
        console.log('client app Id passed..');
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('convertAppAction --> '+state);
            var spinner = component.find("waitSpinner");
            $A.util.addClass(spinner, "slds-hide");
            if(state === "SUCCESS"){
                var result=response.getReturnValue();
                console.log(result);
                component.set("v.lendingApp",result);
                component.set("v.lendingAppId",result.Id);
                component.set("v.finalMessage", 'Application has been created successfully!')
                //component.set("v.isActionCompleted",true);
                console.log(component.get("v.lendingApp"));
                console.log(component.get("v.finalMessage"));
                console.log(component.get("v.isActionCompleted"));
                $A.util.addClass(component.find("confirmMessage"), 'slds-hide');
                $A.util.addClass(component.find("create"), 'slds-hide');
                $A.get("e.force:closeQuickAction").fire();
               var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.lendingAppId"),
                });
                navEvt.fire();
              
            }
            else if(state === "ERROR"){
                component.set("v.error",true);
                $A.util.addClass(component.find("confirmMessage"), 'slds-hide');
                component.set("v.errorMsg",'An error has occured. Please try again.')
                var errors = response.getError();                       
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }  
            }
        });
        $A.enqueueAction(action);
    },
})