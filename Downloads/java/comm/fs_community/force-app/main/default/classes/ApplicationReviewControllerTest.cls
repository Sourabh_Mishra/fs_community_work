@isTest
public class ApplicationReviewControllerTest {
    @testSetup
    private static void createTestData(){
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];
        User adminUser = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' Limit 1];
        adminUser.UserRoleId = userrole.Id;
        update adminUser;
        
        System.runAs(adminUser){
            TestDataHelper.createSetups(adminUser.Id);
            
            Account acc = new Account();
            acc.Name = 'TestAcc';
            insert acc;
            system.debug('Acc--- '+acc);
            
            Contact con = new Contact();
            con.LastName = 'conLN';
            con.AccountId = acc.Id;
            insert con;
            system.debug('Con--- '+con);
            
            Id profileId = [SELECT Id FROM Profile WHERE Name=:CommunityConstants.COMMUNITY_USER_PROFILE].Id;
            
            String uniqueUserName;
            uniqueUserName = 'andy' + DateTime.now().getTime() + '@testorg.com';
            User usr = new User(
                LastName = 'User1',
                FirstName='Test',
                Alias = 'tUOne',
                Email = 'andy.user@test.com',
                Username = uniqueUserName,
                ProfileId = profileId,
                TimeZoneSidKey = 'GMT',
                LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US',
                ContactId = con.Id
            );
            insert usr;
            system.debug('User--- '+usr);
            
            
            Client_Application__c application1 = new Client_Application__c();
            application1.Payment_Cycle__c = 'Annual';
            application1.Product__c = 'Personal Loan';
            application1.Purpose__c = 'Other';
            application1.Loan_Amount__c = 1000;
            
            insert application1;
            
            
            Client_Document__c testProduct = new Client_Document__c(Name = 'testProd');
            insert testProduct;
            ContentVersion testCV = new ContentVersion(Title = 'testFile', PathOnClient = 'TestFile.pdf', VersionData = Blob.valueOf('Test Content'));
            insert testCV;
            Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:testCV.Id].ContentDocumentId;    
            ContentDocumentLink testCDL = new ContentDocumentLink(ContentDocumentId = conDocId,
                                                                  LinkedEntityId = testProduct.Id);
            insert testCDL;
            
            Client_Document__c testProduct1 = new Client_Document__c(Name = 'Mortgage Statement');
            testProduct1.Client_Application__r=application1;
            insert testProduct1;
            ContentVersion testCV1 = new ContentVersion(Title = 'Mortgage Statement', PathOnClient = 'TestFile.pdf', VersionData = Blob.valueOf('Test Content'));
            insert testCV1;
            Id conDocId1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:testCV1.Id].ContentDocumentId;    
            ContentDocumentLink testCDL1 = new ContentDocumentLink(ContentDocumentId = conDocId1,
                                                                   LinkedEntityId = testProduct1.Id);
            insert testCDL1;
            
            Client_Document__c testProduct2 = new Client_Document__c(Name = 'Passport');
            testProduct2.Client_Application__r=application1;
            insert testProduct2;
            ContentVersion testCV2 = new ContentVersion(Title = 'Passport', PathOnClient = 'TestFile.pdf', VersionData = Blob.valueOf('Test Content'));
            insert testCV2;
            Id conDocId2 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:testCV2.Id].ContentDocumentId;    
            ContentDocumentLink testCDL2 = new ContentDocumentLink(ContentDocumentId = conDocId2,
                                                                   LinkedEntityId = testProduct2.Id);
            insert testCDL2;
            
            Client_Document__c testProduct3 = new Client_Document__c(Name = 'Drivers License');
            testProduct3.Client_Application__r=application1;
            insert testProduct3;
            ContentVersion testCV3 = new ContentVersion(Title = 'Drivers License', PathOnClient = 'TestFile.pdf', VersionData = Blob.valueOf('Test Content'));
            insert testCV3;
            Id conDocId3 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:testCV3.Id].ContentDocumentId;    
            ContentDocumentLink testCDL3 = new ContentDocumentLink(ContentDocumentId = conDocId3,
                                                                   LinkedEntityId = testProduct3.Id);
            insert testCDL3;
            
            Client_Document__c testProduct4 = new Client_Document__c(Name = 'Bank Statement');
            testProduct4.Client_Application__r=application1;
            insert testProduct4;
            ContentVersion testCV4 = new ContentVersion(Title = 'Bank Statement', PathOnClient = 'TestFile.pdf', VersionData = Blob.valueOf('Test Content'));
            insert testCV4;
            Id conDocId4 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:testCV4.Id].ContentDocumentId;    
            ContentDocumentLink testCDL4 = new ContentDocumentLink(ContentDocumentId = conDocId4,
                                                                   LinkedEntityId = testProduct4.Id);
            insert testCDL4;
            
            
            
        }
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        
        System.runAs(usr){
            
            Client_Financial__c income = new Client_Financial__c();
            income.Client_Financial_Family__c = 'Income';
            income.Frequency__c = 'Every 2 Weeks';
            income.Monthly_Stated_Amount__c = 50000;
            income.Client_Financial_Type__c = 'Disability';
            income.RecordTypeId = [SELECT Id,Name FROM RecordType WHERE SobjectType='Client_Financial__c' AND Name = 'Income'].Id;
            insert income;
            
            Client_Financial__c liability = new Client_Financial__c();
            liability.Client_Financial_Family__c = 'Liability';
            liability.Frequency__c = 'Every 2 Weeks';
            liability.Monthly_Stated_Amount__c = 5000;
            liability.Client_Financial_Type__c = 'Rent';
            liability.RecordTypeId = [SELECT Id,Name FROM RecordType WHERE SobjectType='Client_Financial__c' AND Name = 'Liability'].Id;
            insert liability;
            
            Client_Employment__c employment = new Client_Employment__c();
            employment.Employer_Name__c = 'Test';
            employment.Department__c = 'engg';
            employment.Employment_Type__c = 'Casual';
            employment.Salary__c = 4000;
            employment.Employment_Reference_Number__c = '5678';
            insert employment;                        
            
            Client_Application__c application = new Client_Application__c();
            application.Payment_Cycle__c = 'Annual';
            application.Product__c = 'Personal Loan';
            application.Purpose__c = 'Other';
            application.Loan_Amount__c = 2000;
            insert application;           
            
        }
    }

/*    @isTest
    public static void testGetDetailsFromApp(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        Client_Document__c cd=[Select Id,Name from Client_Document__c where Name='testProd'];
        
        
        ContentVersion cont=[Select Id from ContentVersion where Title = 'Bank Statement'];
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cont.Id].ContentDocumentId;
        String testContent = 'TestContent';
        ContentDocumentLink CDL=ApplicationReviewController.saveChunk(cd.id,'testFile',testContent, 'txt','',clientApp.Name,'Bank Statement');
        System.assert(CDL!=null); 
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        
        System.runAs(usr){
            Map<String,List<ApplicationReviewController.documentStatus>> docStatus = ApplicationReviewController.getAlreadyUploadedDocuments(clientApp.Name);
            List<String> availableDocs = ApplicationReviewController.fetchAvailableDocs(clientApp.Name);
            Boolean isAppSubmitted = ApplicationReviewController.isSubmitted(clientApp.Name);
            System.assert(isAppSubmitted!=null);
            //  ApplicationReviewController.attachDocuments(clientApp.Name);
            ApplicationReviewController.documentStatus cdocuu=new ApplicationReviewController.documentStatus(); 
            cdocuu.conVersion =ApplicationReviewController.getContentVersion(cd.id) ;
            cdocuu.docStatus = 'Submitted';
            Boolean isStatusSubmitted=ApplicationReviewController.ifStatusIsSubmitted(clientApp.Name);
            System.assert(isStatusSubmitted!=null);
            ApplicationReviewController.deleteAllDocuments(clientApp.Name);
            
        //    Boolean isAllDone= ApplicationReviewController.areAllSubmitted(clientApp.Name);
        //    System.assert(isAllDone!=null);
            Boolean isDone=ApplicationReviewController.documentStatus(clientApp.Name);    
            List<ContentVersion> conver = ApplicationReviewController.getContentVersion(cd.Id);
            // ApplicationReviewController.attachDocuments(clientApp.Name);
            ApplicationReviewController.deleteFile(conDocId);
            ApplicationReviewController.deleteFilesRecord(clientApp.Name);
            System.assert(conver!=null);
        }
        Test.stopTest();
        
    }
*/
    @isTest
    public static void testGetEmployFromApp(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Client_Employment__c employment = [select Id, Name, Employer_Name__c, Department__c, Salary__c FROM Client_Employment__c where Salary__c = 4000 LIMIT 1];
        System.runAs(usr){
            Id empId = ApplicationReviewController.getEmploymentIdFromApplication();
            System.assertEquals(employment.Id, empId);
        }
        Test.stopTest();
        
    }
    @isTest
    public static void testGetContactFromApp(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
            Contact con = ApplicationReviewController.getContactFromApplication();
            ApplicationReviewController.updateContact(con);
            System.assertEquals(contact.LastName, con.LastName);
        }
        Test.stopTest();
        
    }
    @isTest
    public static void testGetFinancialsFromApp(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        Client_Financial__c clientIncome = [SELECT Id, Name, Monthly_Stated_Amount__c FROM Client_Financial__c where Client_Financial_Family__c = 'Income' LIMIT 1];
        Client_Financial__c clientLiability = [SELECT Id, Name, Monthly_Stated_Amount__c FROM Client_Financial__c where Client_Financial_Family__c = 'Liability' LIMIT 1];        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        Id incRecTypeId = [SELECT Id,Name FROM RecordType WHERE SobjectType='Client_Financial__c' AND Name = 'Income'].Id;
        Id liabilityRecTypeId = [SELECT Id,Name FROM RecordType WHERE SobjectType='Client_Financial__c' AND Name = 'Liability'].Id;
        
        System.runAs(usr){
            List<Client_Financial__c> incomeList = ApplicationReviewController.getIncomeRecordsFromApplication();
            System.assertEquals(clientIncome.Monthly_Stated_Amount__c, incomeList[0].Monthly_Stated_Amount__c);
            
            List<Client_Financial__c> liabilityList = ApplicationReviewController.getLiabilityRecordsFromApplication();
            System.assertEquals(clientLiability.Monthly_Stated_Amount__c, liabilityList[0].Monthly_Stated_Amount__c);
            
            Id incomeRecTypeIdResult = ApplicationReviewController.getIncomeRecTypeId();
            System.assertEquals(incRecTypeId, incomeRecTypeIdResult);
            
            Id liabilityRecTypeIdResult = ApplicationReviewController.getLiabilityRecTypeId();
            System.assertEquals(liabilityRecTypeId, liabilityRecTypeIdResult);
        }
        Test.stopTest();
        
    }
    @isTest
    public static void testClientApp(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
            Id clientAppId = ApplicationReviewController.getClientApplications(clientApp.Name);
            Id appId = ApplicationReviewController.getApplicationIdFromName(clientApp.Name);
            System.assertEquals(clientAppId, clientApp.Id);
        }
        Test.stopTest();
        
    }
    
    
    @isTest
    public static void testfetchAvailableDocs(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
            List<String> availableDocs = ApplicationReviewController.fetchAvailableDocs(clientApp.Name);
           
            System.assert(availableDocs!=null);
        }
        Test.stopTest();
        
    }
    
    
      @isTest
    public static void testgetAlreadyUploadedDocuments(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
            Map<String,List<ApplicationReviewController.documentStatus>> docStatus = ApplicationReviewController.getAlreadyUploadedDocuments(clientApp.Name);
           
            System.assert(docStatus!=null);
        }
        Test.stopTest();
        
    }
    
    
    @isTest
    public static void testisSubmitted(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
           Boolean isAppSubmitted = ApplicationReviewController.isSubmitted(clientApp.Name);
           
            System.assert(isAppSubmitted!=null);
        }
        Test.stopTest();
        
    }
    
    
        @isTest
      public static void testdocumentStatus(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
           ApplicationReviewController.documentStatus cdocuu=new ApplicationReviewController.documentStatus();           
           System.assert(cdocuu!=null);
        }
        Test.stopTest();
        
    }
    
    
            @isTest
      public static void testisStatusSubmitted(){
        Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
           Boolean isStatusSubmitted=ApplicationReviewController.ifStatusIsSubmitted(clientApp.Name);
           System.assert(isStatusSubmitted!=null);
        }
        Test.stopTest();
        
    }
    
       @isTest
      public static void testdeleteAllDocuments(){
       Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
          ApplicationReviewController.deleteAllDocuments(clientApp.Name);
          
        }
        Test.stopTest();
        
    }
    

    
   
      @isTest
      public static void testgetContentVersion(){
      Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        Client_Document__c cd=[Select Id,Name from Client_Document__c where Name='testProd']; 
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
         List<ContentVersion> conver = ApplicationReviewController.getContentVersion(cd.Id);   
         System.assert(conver!=null);
        }
        Test.stopTest();
        
    }
    
    
          @isTest
      public static void testdeleteFile(){
      Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        Client_Document__c cd=[Select Id,Name from Client_Document__c where Name='testProd']; 
          
        ContentVersion cont=[Select Id from ContentVersion where Title = 'Bank Statement'];
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cont.Id].ContentDocumentId;
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
        ApplicationReviewController.deleteFile(conDocId); 
         
        }
        Test.stopTest();
        
    } 

    
       @isTest
      public static void testdeleteFilesRecord(){
      Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        Client_Document__c cd=[Select Id,Name from Client_Document__c where Name='testProd']; 
          
        ContentVersion cont=[Select Id from ContentVersion where Title = 'Bank Statement'];
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cont.Id].ContentDocumentId;
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
        ApplicationReviewController.deleteFilesRecord(clientApp.Name);
         
        }
        Test.stopTest();
        
    }
    
    
   /*  @isTest
      public static void testattachDocs(){
       Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
         ApplicationReviewController.attachDocuments(clientApp.Name);
          
        }
        Test.stopTest();
        
    }*/
    
    
         @isTest
      public static void testaddAllClear(){
       Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
        
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
        System.runAs(usr){
        Boolean isAllClear= ApplicationReviewController.addAllClear(clientApp.Name);
        System.assert(isAllClear!=null); 
        }
        Test.stopTest();
        
    }
    
    
    
      @isTest
      public static void testsaveChunk(){
       Client_Application__c clientApp = [select Id, Name
                                           ,Loan_Amount__c
                                           FROM Client_Application__c where Payment_Cycle__c = 'Annual' and Loan_Amount__c=2000];
       Client_Document__c cd=[Select Id,Name from Client_Document__c where Name='testProd'];
        
        
        ContentVersion cont=[Select Id from ContentVersion where Title = 'Bank Statement'];
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cont.Id].ContentDocumentId;
        String testContent = 'TestContent';
        ContentDocumentLink CDL=ApplicationReviewController.saveChunk(cd.id,'testFile',testContent, 'txt','',clientApp.Name,'Bank Statement');
        System.assert(CDL!=null); 
        Test.startTest();
        User usr = [Select Id FROM User WHERE Alias='tUOne'];
        Contact contact = [select Id, FirstName, LastName FROM Contact where LastName = 'conLN' LIMIT 1];
       /* System.runAs(usr){
        ContentDocumentLink CDL=ApplicationReviewController.saveChunk(cd.id,'testFile',testContent, 'txt','',clientApp.Name,'Bank Statement');
        System.assert(CDL!=null); 
        }*/
        Test.stopTest();
        
    }
    

}