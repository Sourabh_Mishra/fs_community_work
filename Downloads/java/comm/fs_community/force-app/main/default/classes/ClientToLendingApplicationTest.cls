@isTest
public class ClientToLendingApplicationTest {
    @testSetup
    public static void createTestData(){
        TestDataHelper.createLendingAppTestData();
    }
    
    @isTest
    public static void testConvertClientApp(){
        Id profileId = [SELECT Id FROM Profile WHERE Name='Custom Community Plus User'].Id;
        User commUser = [SELECT Id, Name from User where ProfileId = :profileId LIMIT 1];
        Client_Application__c testClientApp = [SELECT Id, Name
                                               ,Loan_Amount__c
                                               ,Payment_Cycle__c
                                               ,Primary_Customer_Account__c
                                               ,Primary_Customer_Contact__c
                                               ,Product_Family__c 
                                               ,Purpose__c
                                               ,Status__c
                                               ,Application_Date__c
                                               ,OwnerId
                                               ,Product__c
                                               ,LastModifiedDate
                                               from Client_Application__c where Loan_Amount__c = 1000 LIMIT 1];
        System.debug('ClientAppCreated---'+testClientApp);
        
        
        User adminUser = [select id, Name from User where LastName = 'adminUser'];
        System.runAs(adminUser){
            
            fsCore__Document_Setup__c cdocs=new fsCore__Document_Setup__c();
            cdocs.fsCore__Is_Active__c=true;
            cdocs.fsCore__Default_Company_Availability__c='Available';
            cdocs.fsCore__Default_Product_Availability__c='Available';
            cdocs.fsCore__Default_Selection_Rule_Result__c='Available';
            cdocs.Name='Bank Statement';
            cdocs.fsCore__Document_Classification__c='Bank Statement';
            Integer randomNumber = Integer.valueof((Math.random() * 100));
            cdocs.fsCore__Document_Code__c='Bank Statement'+randomNumber;
            cdocs.fsCore__Is_Lease__c=true;
            cdocs.fsCore__Selection_Order__c=0;
            cdocs.fsCore__Line_Of_Business__c='Origination';
            cdocs.fsCore__Access_Level__c='Edit';
            insert cdocs;
            
            Client_Document__c cdocus=new Client_Document__c();
            //cdocus.Name=testClientApp.Id;
            cdocus.Name='Bank Statement'+'('+testClientApp.Name+')';
            cdocus.Client_Application_Number__c=testClientApp.Name;
            cdocus.Status__c='Submitted';
            cdocus.Type__c='Bank Statement';
            cdocus.Document_Classification__c='Bank Statement';
            insert cdocus;
            
            
            ContentVersion testCV = new ContentVersion(Title = 'testFile', PathOnClient = 'TestFile.pdf', VersionData = Blob.valueOf('Test Content'));
            insert testCV;
            Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:testCV.Id].ContentDocumentId;    
            ContentDocumentLink testCDL = new ContentDocumentLink(ContentDocumentId = conDocId,
                                                                  LinkedEntityId = cdocus.Id);
            insert testCDL;
            
            Test.startTest();
            fsCore__Lending_Application__c testLendingApp = ClientToLendingApplication.convertClientApp(testClientApp.Id);
            Test.stopTest();
            System.assert(testLendingApp!=null);
            System.assertEquals(testLendingApp.fsCore__Loan_Amount__c, 1000);           
        }
    }
    
}