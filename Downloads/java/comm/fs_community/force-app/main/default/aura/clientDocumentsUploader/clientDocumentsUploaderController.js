({
    handleInit : function(component, event, helper) {
        helper.doInit(component, event, helper); 
    },
    eventFired:function(component,event,helper){
        console.log('Event is Fired');
        helper.doInit(component, event, helper);
    },
    preview : function(component, event, helper) {
        $A.get('e.lightning:openFiles').fire({
            recordIds: [event.currentTarget.getAttribute("data-Id")]
        });
    },

    deleteSelectedFile : function(component, event, helper){
        helper.deleteUploadedFile(component, event);  
        component.set("v.isOpen", false); 
        helper.doInit(component, event, helper); 
    },
    openModel: function(component, event, helper) {
        component.set("v.selectedId" , event.currentTarget.getAttribute("data-Id"));
        component.set("v.isOpen", true);
    },
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
 
    
    handleOptionSelected: function(component, event, helper) {
        var selectedDocType=component.find("selectItem").get("v.value");
        component.set("v.selectedDocType",selectedDocType);
        console.log('Selected document type is:'+selectedDocType);
        var selectedDocTypes=document.getElementById('upload');
        selectedDocTypes.disabled=!selectedDocTypes;
        var btn =document.getElementById('doc');
      },
    
    handleClick: function(component, event, helper) {
        var docbutton=document.getElementById('doc');
        docbutton.disabled=true;
        helper.attachDocument(component,event,helper);
        helper.doInit(component, event, helper); 
    },
    
    handleDelete: function(component,event,helper)
    {
        helper.deleteAllDocuments(component,event,helper);
        helper.doInit(component,event,helper);
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.loadingSpinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.loadingSpinner", false);
    },
    
    
})