@isTest
public class docStatusChangeTest {
    
    
     @testSetup
    private static void createTestData(){
       TestDataHelper.createLendingAppTestData();
    }
    
      @isTest
    private static void testDocs()
    {
       
        Client_Application__c testClientApp = [SELECT Id, Name
                                               ,Loan_Amount__c
                                               ,Payment_Cycle__c
                                               ,Primary_Customer_Account__c
                                               ,Primary_Customer_Contact__c
                                               ,Product_Family__c 
                                               ,Purpose__c
                                               ,Status__c
                                               ,Application_Date__c
                                               ,OwnerId
                                               ,Product__c
                                               ,LastModifiedDate
                                               from Client_Application__c where Purpose__c = 'Other' limit 1];
        System.debug('ClientAppCreated---'+testClientApp);
        User adminUser = [select id, Name from User where LastName = 'adminUser'];
        System.runAs(adminUser){
        
        fsCore__Lending_Application__c testLendingApp = ClientToLendingApplication.convertClientApp(testClientApp.Id);
       
        System.assert(testLendingApp!=null);
        System.assertEquals(testLendingApp.fsCore__Loan_Amount__c, 1000);
            
         
            
        fsCore__Document_Setup__c cdocs=new fsCore__Document_Setup__c();
        cdocs.fsCore__Is_Active__c=true;
        cdocs.fsCore__Default_Company_Availability__c='Available';
        cdocs.fsCore__Default_Product_Availability__c='Available';
        cdocs.fsCore__Default_Selection_Rule_Result__c='Available';
        cdocs.Name='Bank Statement';
        cdocs.fsCore__Document_Classification__c='Bank Statement';
        Integer randomNumber = Integer.valueof((Math.random() * 100));
        cdocs.fsCore__Document_Code__c='Bank Statement'+randomNumber;
        cdocs.fsCore__Is_Lease__c=true;
        cdocs.fsCore__Selection_Order__c=0;
        cdocs.fsCore__Line_Of_Business__c='Origination';
        cdocs.fsCore__Access_Level__c='Edit';
        insert cdocs;
            
        Client_Document__c cdocus=new Client_Document__c();
        //cdocus.Name=testClientApp.Id;
        cdocus.Name='Bank Statement'+'('+testClientApp.Name+')';
        cdocus.Client_Application_Number__c=testClientApp.Name;
        cdocus.Status__c='Submitted';
        cdocus.Type__c='Bank Statement';
        cdocus.Document_Classification__c='Bank Statement';
        insert cdocus;
            
          //   List<Client_Document__c> cdocc =[Select Status__c,OwnerId,Client_Application_Number__c from Client_Document__c where Client_Application_Number__c=:testClientApp.Name AND Type__c=:'Bank Statement'];
        
        fsCore__Lending_Document_Record__c lendingDocs=new fsCore__Lending_Document_Record__c();
           // fsCore__Document_Name__c
        lendingDocs.fsCore__Document_Name__c=cdocs.Id;
        lendingDocs.fsCore__Document_Classification__c='Bank Statement';
        lendingDocs.fsCore__Status__c='Submitted';
        lendingDocs.fsCore__Lending_Application_Number__c=testLendingApp.Id;
        insert lendingDocs;
            
         fsCore__Lending_Document_Record__c status=[Select id,fsCore__Status__c from fsCore__Lending_Document_Record__c where fsCore__Document_Classification__c='Bank Statement'];
         status.fsCore__Status__c='Rejected';
         update status;
            
         
   
        
        }}
        
}